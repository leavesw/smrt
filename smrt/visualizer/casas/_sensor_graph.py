###############################################################################
# Copyright (c) 2018-2019, Tinghui Wang
# License: BSD 3-Clause
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###############################################################################

"""This file contains routines for plotting sensor adjacency and relationship
on the smart home floor plan for visualization.
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from pycasas.data import CASASDataset
from ._helper import prepare_floorplan


def plot_sensor_graph(dataset, adjacency_matrix,
                      show_sensor_name=False, title=None, filename=None):
  """Plot sensor accessibility graph based on sensor embeddings

  Plot sensor graph (weighted directed graph) using sensor embeddings
  calculated given threshold. The sensor pair whose distance is below the
  given threshold is considered adjacent (connected by an edge in the graph).

  Args:
    dataset (:obj:`~pymrt.casas.CASASDataset`): The dataset the embeddings
      are calculated.
    adjacency_matrix (:obj:`numpy.ndarray`): Sensor adjacency matrix of
      shape `[num_sensors, num_sensors]`.
    show_sensor_name: bool, True if sensor name is shown on the graph.
    title (:obj:`str`): The name of the graph.
    filename (:obj:`str`): The name of the file to save the graph.

  Returns:
      :obj:`~matplotlib.figure.Figure` object so that the plot can be embedded
      in `tinker` application.
  """
  assert (isinstance(dataset, CASASDataset))
  # Calculate the distance and determine the adjancency matrix between
  # sensors.
  drawing_data = prepare_floorplan(dataset)
  sensor_list = [sensor['name'] for sensor in dataset.sensor_list]

  fig, ax = plt.subplots(figsize=(10, 10))
  ax.imshow(drawing_data['img'])
  active_patch_list = []
  # Draw line connecting targeted sensors
  for i in range(adjacency_matrix.shape[0]):
    sensor_i = sensor_list[i]
    sensor_i_patch = drawing_data['sensor_boxes'][sensor_i]
    width = sensor_i_patch.get_width()
    height = sensor_i_patch.get_height()
    for j in range(adjacency_matrix.shape[1]):
      if i != j:
        sensor_j = sensor_list[j]
        sensor_j_patch = drawing_data['sensor_boxes'][sensor_j]
        if adjacency_matrix[i, j]:
          ax.add_line(mlines.Line2D(
            xdata=[sensor_i_patch.get_x() + width/2,
                   sensor_j_patch.get_x() + width/2],
            ydata=[sensor_i_patch.get_y() + height/2,
                   sensor_j_patch.get_y() + height/2],
            color='c', linestyle='-', zorder=1, lw=2
          ))
  # Draw sensor blocks
  for key, patch in drawing_data['sensor_boxes'].items():
    if key in sensor_list:
      ax.add_patch(patch)
      active_patch_list.append(patch)
  # Draw sensor annotations
  if show_sensor_name:
    for key, text_data in drawing_data['sensor_texts'].items():
      if key in sensor_list:
        ax.text(*text_data, horizontalalignment='center',
                verticalalignment='top', zorder=3)
  # Show figure
  if title is None:
    title = dataset.get_name() + \
            ' sensor graph'
  fig.suptitle = title
  fig.tight_layout()
  if filename is not None:
    plt.savefig(filename)
  return fig

def sensor_graph_pixelmap(transition_matrix,
                          sensor_list=None,
                          annotation_fontsize=8,
                          cmap='GnBu',
                          alpha=0.5,
                          legend=True,
                          title=None, filename=None):
  """Draw the distance between sensors as a pixel map.

  The distance between sensors is encoded as intensity of the corresponding
  pixel cell.

  Args:
      distance_matrix (:obj:`numpy.ndarray`): A `ndarray` of shape
          `[num_sensors, num_sensors]` where each element is the euclidean
          or cosine distance between two sensors.
      sensor_list (:obj:`list`): List of sensor dictionary where 'name' key is
          mapped to the sensor ID.
      annotation_fontsize (:obj:`int`): Font size of the annotation on the
          pixel image.
      legend (:obj:`bool`): Plot color bar on the image generated.
      title (:obj:`str`): Title of the plot
      filename (:obj:`str`): Name of the file to save the drawing to.
  """
  fig, ax = plt.subplots(figsize=(10, 10))
  im = ax.imshow(transition_matrix, interpolation='none',
                 cmap=cmap, alpha=alpha)
  if annotation_fontsize != 0:
    for i in range(transition_matrix.shape[0]):
      for j in range(transition_matrix.shape[1]):
        ax.annotate('%.2f' % transition_matrix[i, j],
                    fontsize=annotation_fontsize,
                    xy=(j, i), xycoords='data', xytext=(j, i),
                    horizontalalignment='center')
  # Move xaxis tick to the top
  ax.xaxis.tick_top()
  # Move xaxis label to the top as well
  ax.xaxis.set_label_position('top')
  # Each tick at the center
  ax.set_xticks(np.arange(transition_matrix.shape[1]), minor=False)
  ax.set_yticks(np.arange(transition_matrix.shape[1]), minor=False)
  # Prepare sensor label
  if sensor_list is None:
    sensor_labels = ['s_%d' % i for i in range(transition_matrix.shape[1])]
  else:
    sensor_labels = [
      sensor_list[i]['name'] for i in range(transition_matrix.shape[1])
    ]
  ax.set_xticklabels(sensor_labels, horizontalalignment='left',
                     minor=False, rotation=45)
  ax.set_yticklabels(sensor_labels, minor=False, rotation=0)
  if legend:
    plt.colorbar(im, fraction=0.046, pad=0.04)
  if title is None:
    fig.suptitle('Sensor distance')
  else:
    fig.suptitle(title)
  plt.tight_layout()
  if filename is None:
    plt.show()
  else:
    plt.savefig(filename)
