###############################################################################
# Copyright (c) 2018-2019, Tinghui Wang
# License: BSD 3-Clause
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###############################################################################
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation


def plot2d_data_preparation(prediction=None,
                            observation=None,
                            truth=None):
  """Prepare data for 2D Plot

  Args:
    prediction: List of predictions. The prediction is a dictionary with key
      `locations` and `targets`.
    observation: List of observations at each time step. The observations at
      each time step is a numpy array of shape `[num_obs, n_visible]`.
    truth: The ground truth of the state of each target. The truth is a
      dictionary with key `locations` and `targets`. The true locations at each
      time step is a numpy array of shape `[num_targets, ndims]`, and the
      targets of each step is a numpy array of shape `[num_targets,]` filled
      with corresponding target ID.

  Returns:
    (:obj:`dict`): Dictionary containing a plot with lines connecting
      observation and truth in the same frame. The dictionary contains
      the following keys:

      * ``fig``: matplotlib Figure object.
      * ``ax``: matplotlib Axes.
      * ``all``: List of all :obj:`~matplotlib.lines.Line2D` drawn in
          the figure.
      * ``observation``: List of :obj:`~matplotlib.lines.Line2D`
          indexed by frame.
      * ``truth``: List of :obj:`~matplotlib.lines.Line2D`
          indexed by frame.
  """
  fig, (ax) = plt.subplots(1, 1)
  obs_line_list = None
  all_lines = []
  all_annotations = []
  if observation is not None:
    obs_line_list = []
    # Process Observation
    plot_data = observation
    for i in range(len(plot_data)):
      observation_x = plot_data[i][:, 0]
      observation_y = plot_data[i][:, 1]
      cur_line_group = ax.plot(observation_x, observation_y, marker='s',
                               ls='None', color='g', markersize=10)
      obs_line_list.append(cur_line_group)
      all_lines += cur_line_group
  # Process Truth if provided
  truth_line_list = None
  truth_annotation_list = None
  if truth is not None:
    truth_annotation_list = []
    truth_line_list = []
    plot_data = truth["locations"]
    for i in range(len(plot_data)):
      truth_x = plot_data[i][:, 0]
      truth_y = plot_data[i][:, 1]
      cur_line_group = ax.plot(truth_x, truth_y, marker='o',
                               ls='None', color='r', markersize=10)
      truth_line_list.append(cur_line_group)
      all_lines += cur_line_group
      current_annotation = []
      for j in range(len(truth['targets'][i])):
        current_annotation.append(
          ax.annotate(
            '%d' % truth['targets'][i][j],
            (truth_x[j], truth_y[j]),
            color='r',
            bbox={
              'boxstyle': 'round',
              'fc': 'w',
              'alpha': 0.5
            }
          )
        )
      all_annotations += current_annotation
      truth_annotation_list.append(current_annotation)
  # Process prediction if provided
  pred_line_list = None
  pred_annotation_list = None
  if prediction is not None:
    pred_line_list = []
    pred_annotation_list = []
    plot_data = prediction["locations"]
    for i in range(len(plot_data)):
      prediction_x = plot_data[i][:, 0]
      prediction_y = plot_data[i][:, 1]
      cur_line_group = ax.plot(prediction_x, prediction_y, marker='^',
                               ls='None', color='b', markersize=10)
      pred_line_list.append(cur_line_group)
      all_lines += cur_line_group
      current_annotation = []
      for j in range(prediction['targets'][i].shape[0]):
        current_annotation.append(
          ax.annotate(
            '%d' % prediction['targets'][i][j],
            (prediction_x[j], prediction_y[j]),
            color='b',
            bbox={
              'boxstyle': 'round',
              'fc': 'w',
              'alpha': 0.5
            }
          )
        )
      all_annotations += current_annotation
      pred_annotation_list.append(current_annotation)
  return {
    'fig': fig,
    'ax': ax,
    'all': all_lines,
    'observation': obs_line_list,
    'truth': truth_line_list,
    'prediction': pred_line_list,
    'annotation': all_annotations,
    'truth_annotation': truth_annotation_list,
    'prediction_annotation': pred_annotation_list,
  }


def plot2d_truth(model, truth):
  """ Plot truth on 2D plane

  Args:
      truth (:obj:`tuple`): A tuple of truth states (by step)
      model (:obj:`pymrt.tracking.models.CVModel`):
  """
  plot_data = plot2d_data_preparation(truth=truth, model=model)
  truth_line_list = plot_data['truth']
  all_lines = plot_data['all']
  fig = plot_data['fig']
  num_steps = len(truth_line_list)

  # Animation Update Routine
  def truth_plot_init():
    for line_group in truth_line_list:
      for line in line_group:
        line.set_alpha(0)
    return all_lines

  def truth_plot_update(frame):
    for i in range(frame, max(0, frame - 8), -1):
      for line in truth_line_list[i]:
        line.set_alpha(1 - 0.1 * (frame - i))
    return all_lines

  # Start animation
  ani = FuncAnimation(fig, truth_plot_update,
                      frames=range(num_steps),
                      init_func=truth_plot_init, blit=True, interval=500)
  plt.show()


def plot2d_observation(observation, truth=None):
  """ Plot truth on 2D plane

  Args:
      observation (:obj:`list`): List of observations.
      truth (:obj:`tuple`): A tuple of truth states (by step)
      model (:obj:`pymrt.tracking.models.CVModel`):

  Returns:
    The function animator, which can be used for inline display in jupyter
    notebook.
  """
  plot_data = plot2d_data_preparation(
    observation=observation, truth=truth
  )
  truth_line_list = plot_data['truth']
  obs_line_list = plot_data['observation']
  all_lines = plot_data['all']
  fig = plot_data['fig']
  num_steps = len(obs_line_list)

  # Animation Update Routine
  def plot_init():
    for line in all_lines:
      line.set_alpha(0)
    return all_lines

  def plot_update(frame):
    for i in range(frame, max(0, frame - 8), -1):
      for line in obs_line_list[i]:
        line.set_alpha(1 - 0.1 * (frame - i))
      if truth is not None:
        for line in truth_line_list[i]:
          line.set_alpha(1 - 0.1 * (frame - i))
    return all_lines

  # Start animation
  ani = FuncAnimation(fig, plot_update,
                      frames=range(num_steps),
                      init_func=plot_init, blit=True, interval=500)
  return ani


def animate2d_prediction(prediction=None,
                         truth=None,
                         observation=None):
  """ Plot truth on 2D plane

  Args:
      observation (:obj:`list`): List of observations.
      truth (:obj:`tuple`): A tuple of truth states (by step)
      prediction

  Returns:
    The function animator, which can be used for inline display in jupyter
    notebook.
  """
  plot_data = plot2d_data_preparation(
    prediction=prediction, observation=observation, truth=truth
  )
  truth_line_list = plot_data['truth']
  obs_line_list = plot_data['observation']
  prediction_line_list = plot_data['prediction']
  all_lines = plot_data['all']
  all_annotations = plot_data['annotation']
  truth_annotation_list = plot_data['truth_annotation']
  pred_annotation_list = plot_data['prediction_annotation']
  fig = plot_data['fig']
  num_steps = len(obs_line_list)

  def hide_annotation(annotation):
    annotation.set_alpha(0)
    annotation.get_bbox_patch().set_alpha(0)

  def show_annotation(annotation):
    annotation.set_alpha(1)
    annotation.get_bbox_patch().set_alpha(0.5)

  # Animation Update Routine
  def plot_init():
    for line in all_lines:
      line.set_alpha(0)
    for annotation in all_annotations:
      hide_annotation(annotation)
    return all_lines + all_annotations

  def plot_update(frame):
    for i in range(frame, max(0, frame - 8), -1):
      for line in obs_line_list[i]:
        line.set_alpha(1 - 0.1 * (frame - i))
      if truth is not None:
        for line in truth_line_list[i]:
          line.set_alpha(1 - 0.1 * (frame - i))
      if prediction is not None:
        for line in prediction_line_list[i]:
          line.set_alpha(1 - 0.1 * (frame - i))
    if frame > 0:
      if truth_annotation_list is not None:
        for annotation in truth_annotation_list[frame - 1]:
          hide_annotation(annotation)
      if pred_annotation_list is not None:
        for annotation in pred_annotation_list[frame - 1]:
          hide_annotation(annotation)
    if truth_annotation_list is not None:
      for annotation in truth_annotation_list[frame]:
        show_annotation(annotation)
    if pred_annotation_list is not None:
      for annotation in pred_annotation_list[frame]:
        show_annotation(annotation)
    return all_lines + all_annotations

  # Start animation
  ani = FuncAnimation(fig, plot_update,
                      frames=range(num_steps),
                      init_func=plot_init, blit=True, interval=500)
  return ani

# def plot2d_gmphd(gm_weights,
#                  gm_means,
#                  gm_covs,
#                  truth=None,
#                  observations=None,
#                  grid=None,
#                  title="PHD with current observations",
#                  num_contours=10,
#                  log_scale=False):
#   """ Animate GM-PHD Filter result on 2D plot with matplotlib
#
#   Args:
#     gm_weights:
#     gm_means:
#     gm_covs:
#     truth:
#     observations:
#     grid:
#     title:
#     num_contours:
#     log_scale:
#   """
#   fig, (ax) = plt.subplots(1, 1)
#   if grid is None:
#     xlim_min = np.min(gm_means[:, 0])
#     xlim_max = np.max(gm_means[:, 0])
#     ylim_min = np.min(gm_means[:, 1])
#     ylim_max = np.max(gm_means[:, 1])
#     xdev = np.sqrt(np.max(np.abs(gm_covs[:, 0, 0])))
#     ydev = np.sqrt(np.max(np.abs(gm_covs[:, 1, 1])))
#     grid = np.mgrid[
#        xlim_min-xdev:xlim_max+xdev:25j,
#        ylim_min-ydev:ylim_max+ydev:25j
#     ]
#   phd = gm_calculate(gm_weights=gm_weights,
#                      gm_means=gm_means,
#                      gm_covs=gm_covs,
#                      grid=grid,
#                      approx_mode=False)
#   if log_scale:
#     phd = np.log(phd + np.finfo(np.float).tiny)
#   contour = ax.contourf(
#     grid[0], grid[1], phd, alpha=0.7
#   )
#   if observations is not None:
#     ax.plot(observations[:, 0], observations[:, 1], marker='s', ls='None',
#             color='g', markersize=10, label="Observations")
#   if truth is not None:
#     ax.plot(truth[:, 0], truth[:, 1], marker='o', ls='None', color='r', markersize=10,
#             label="Truth")
#   plt.colorbar(contour)
#   plt.legend()
#   plt.show()
