###############################################################################
# Copyright (c) 2018-2019, Tinghui Wang
# License: BSD 3-Clause
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###############################################################################
import numpy as np
from scipy.spatial import distance

def normalize_embeddings(embeddings):
  """Normalize sensor embeddings to unit distance from origin

  Args:
    embeddings (:obj:`numpy.ndarray`): A `ndarray` of shape
      `[num_sensors, dim]` that translates each sensor into a vector
      embedding.

  Returns:
    A `ndarray` of shape `[num_sensors, dim]` where each sensor is located on
    the surface of a unit sphere.
  """
  norm = np.linalg.norm(embeddings, axis=1, keepdims=True)
  return embeddings / norm


def euclidean_distance_matrix(embeddings):
  """Get euclidean distance matrix based on embeddings

  Args:
    embeddings (:obj:`numpy.ndarray`): A `ndarray` of shape
      `[num_sensors, dim]` that translates each sensor into a vector
      embedding.

  Returns:
    A `ndarray` of shape `[num_sensors, num_sensors]` where each element is
    the euclidean distance between two sensors.
  """
  num_sensors = embeddings.shape[0]
  distance_matrix = np.zeros((num_sensors, num_sensors), dtype=np.float32)

  for i in range(num_sensors):
    for j in range(num_sensors):
      distance_matrix[i, j] = distance.euclidean(
        embeddings[i, :], embeddings[j, :]
      )
  return distance_matrix


def cosine_distance_matrix(embeddings):
  """Get cosine distance matrix based on embeddings

  Args:
    embeddings (:obj:`numpy.ndarray`): A `ndarray` of shape
      `[num_sensors, dim]` that translates each sensor into a vector
      embedding.

  Returns:
    A `ndarray` of shape `[num_sensors, num_sensors]` where each element is
    the cosine distance between two sensors.
  """
  num_sensors = embeddings.shape[0]
  distance_matrix = np.zeros((num_sensors, num_sensors), dtype=np.float32)

  for i in range(num_sensors):
    for j in range(num_sensors):
      distance_matrix[i, j] = distance.cosine(
        embeddings[i, :], embeddings[j, :]
      )
  return distance_matrix


def cartesian2spherical(xyz):
  """Translate 3D Cartesian to Spherical Coordinates

  Args:
    xyz: np.ndarray, Cartesian coordinates, of shape `[npoints, 3]`.

  Returns:
    np.ndarray, Spherical Coordinates, of shape `[npoints, 3]`, rho, theta, phi
  """
  rtp = np.zeros(xyz.shape)
  xy = xyz[:, 0] ** 2 + xyz[:, 1] ** 2
  rtp[:, 0] = np.sqrt(xy + xyz[:, 2] ** 2)
  # for elevation angle defined from Z-axis down
  rtp[:, 1] = np.arctan2(np.sqrt(xy), xyz[:,2])
  # for elevation angle defined from XY-plane up
  rtp[:, 2] = np.arctan2(xyz[:, 1], xyz[:, 0])
  return rtp
