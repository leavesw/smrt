"""depricated - no long in use"""
###############################################################################
# Copyright (c) 2018-2019, Tinghui Wang
# License: BSD 3-Clause
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###############################################################################

""" This file provides several classes for generate random numbers/vectors
for error estimation/simulation.
"""

import abc
import numpy as np

class RandomGenerator:
    """Abstract class for random number/vector generation
    """
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def generate(self):
        r""" Generate a random sample
        """
        raise NotImplementedError()


class UniformGenerator(RandomGenerator):
    """ The class used for generating data according to a uniform
    distribution in a square.
    """
    def __init__(self, low, high, n):
        self.low = np.array(low).reshape((n, 1))
        self.high = np.array(high).reshape((n, 1))
        self.dim = n

    def generate(self):
        return np.random.uniform(self.low, self.high)


class NormalGenerator(RandomGenerator):
    """ NormalGenerator generate random vector from a multi-variate normal
    disribution.
    """
    def __init__(self, mean, cov, n):
        self.mean = np.array(mean).reshape((n, 1))
        self.cov = np.array(cov).reshape((n, n))
        self.n = n

    def generate(self):
        return np.random.multivariate_normal(
            self.mean.reshape((self.n,)), self.cov.T
        ).reshape((self.n, 1))


class PoissonGenerator(RandomGenerator):
    """ Poisson Distribution Generator
    """
    def __init__(self, lam):
        self.lam = lam

    def generate(self):
        return np.random.poisson(lam=self.lam)
