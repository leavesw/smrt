###############################################################################
# Copyright (c) 2018-2019, Tinghui Wang
# License: BSD 3-Clause
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###############################################################################

"""GMHdf5 Class to R/W list of Gaussian mixtures into HDF5 dataset file.
"""

import os
import h5py
import numpy as np


class GMHdf5:
  """GMHdf5 Class

  Args:
    filename: str, filename of the HDF5 dataset.
    length: int, total length of the Gaussian mixtures to save.
    ndims: int, dimensionality of the Gaussian mixture vector space.
    chunk_size: int, size of the data chunks for faster access.
    Jmax: int, maximum number of Gaussian components in a Gaussian mixture.
    mode: str, 'r' to read from the file; 'w' to write to disk.
  """
  def __init__(self,
               filename,
               mode='r',
               length=0,
               ndims=3,
               Jmax=1000,
               chunk_size=10,
               **kwargs):
    if mode == 'r':
      self._load_from(filename, **kwargs)
    else:
      self._create_hdf5(filename, length, ndims, Jmax, chunk_size, **kwargs)

  def _load_from(self, filename, **kwargs):
    """Load Gaussian mixtures from HDF5 dataset

    Args:
      filename: str, filename of the HDF5 dataset.
    """
    if os.path.isfile(filename):
      self.file = h5py.File(filename, 'r', **kwargs)
      self.Jmax = self.file.attrs['Jmax']
      self.length = self.file.attrs['length']
      self.ndims = self.file.attrs['ndims']
      self.chunk_size = self.file.attrs['chunk_size']
      self.gm_weights = self.file['gm_weights']
      self.gm_means = self.file['gm_means']
      self.gm_covs = self.file['gm_covs']
    else:
      raise FileNotFoundError('File %s is not found.' % filename)

  def _create_hdf5(self, filename, length, ndims, Jmax, chunk_size, **kwargs):
    """Load Gaussian mixtures from HDF5 dataset

    Args:
      filename: str, filename of the HDF5 dataset.
      length: int, total length of the Gaussian mixtures to save.
      ndims: int, dimensionality of the Gaussian mixture vector space.
      Jmax: int, maximum number of Gaussian components in a Gaussian mixture.
      chunk_size: int, size of the data chunks for faster access.
    """
    self.file = h5py.File(filename, 'w', **kwargs)
    self.file.attrs.create('length', length)
    self.file.attrs.create('ndims', ndims)
    self.file.attrs.create('Jmax', Jmax)
    self.file.attrs.create('chunk_size', chunk_size)
    self.gm_weights = self.file.create_dataset('gm_weights',
                                               shape=(length, Jmax),
                                               dtype='f',
                                               chunks=(chunk_size, Jmax))
    self.gm_means = self.file.create_dataset('gm_means',
                                             shape=(length, Jmax, ndims),
                                             dtype='f',
                                             chunks=(chunk_size, Jmax, ndims))
    self.gm_covs = self.file.create_dataset(
      'gm_covs',
       shape=(length, Jmax, ndims, ndims),
       dtype='f',
       chunks=(chunk_size, Jmax, ndims, ndims)
    )
    self.Jmax = Jmax
    self.ndims = ndims
    self.chunk_size = chunk_size
    self.length = length

  def get_attr(self, name):
    """Get HDF5 attributes"""
    if name in self.file.attrs:
      return self.file.attrs[name]
    else:
      raise KeyError('Attribute %s not found.' % name)

  def set_attr(self, name, value):
    """Set HDF5 attributes"""
    if name in self.file.attrs:
      self.file.attrs[name] = value
    else:
      self.file.attrs.create(name=name, data=value)
    self.file.flush()

  def save_gm(self, k, gm_weights, gm_means, gm_covs):
    """Save Gaussian mixtures to file

    Args:
      k: int, the time steps to save the Gaussian mixtures to.
      gm_weights: Weights of the Gaussian components in the Guassian mixtures,
        or a list of weights, each of shape `[n_gc,]`.
      gm_means: Means of the Gaussian components in the Gaussian mixtures,
        or a list of means, each of shape `[n_gc, ndims]`.
      gm_covs: Covariances of the Gaussian components in the Gaussian mixtures,
        or a list of covariance matrices, each of shape `[n_gc, ndims, ndims]`.
    """
    if isinstance(gm_weights, list):
      # Check for others
      assert isinstance(gm_means, list), "Means provided is not a list."
      assert isinstance(gm_covs, list), "Covariance matrices provided is not " \
                                        "a list."
      # Check length
      n_steps = len(gm_weights)
      assert n_steps == len(gm_means), "Length of the means does not match " \
                                       "length of the weights provided"
      assert n_steps == len(gm_covs), "Length of the covariance matrices " \
                                      "does not match the length of the " \
                                      "weights provided"
      for i in range(n_steps):
        self.save_single_gm(k+i, gm_weights[i], gm_means[i], gm_covs[i])
    else:
      self.save_single_gm(k, gm_weights, gm_means, gm_covs)

  def save_single_gm(self, k, gm_weights, gm_means, gm_covs):
    """Save single Gaussian mixture to file

    Args:
      k: int, the time steps to save the Gaussian mixtures to.
      gm_weights: Weights of the Gaussian components in a Guassian mixture,
        of shape `[n_gc,]`.
      gm_means: Means of the Gaussian components in a Gaussian mixtures, of
        shape `[n_gc, ndims]`.
      gm_covs: Covariances of the Gaussian components in a Gaussian mixtures,
        of shape `[n_gc, ndims, ndims]`.
    """
    n_gc = gm_weights.shape[0]
    assert n_gc == gm_means.shape[0], "Number of Gaussian components " \
                                      "mismatch between weights and means."
    assert n_gc == gm_covs.shape[0], "Number of Gaussian components " \
                                     "mismatch between weights and " \
                                     "covariance matrices."
    self.gm_weights[k, :n_gc] = gm_weights
    self.gm_means[k, :n_gc, :] = gm_means
    self.gm_covs[k, :n_gc, :, :] = gm_covs
    if self.Jmax > n_gc:
      self.gm_weights[k, n_gc:] = 0.
      self.gm_means[k, n_gc:, :] = np.zeros((self.ndims,))
      self.gm_covs[k, n_gc:, :, :] = np.zeros((self.ndims, self.ndims))

  def get_gms(self, index):
    """Get Gaussian mixture given the indices

    Args:
      index: the index or list of indexes of the Gaussian mixtures to fetch.

    Returns:
      weights, means and covariance matrices of the Gaussian mixtures requested.
    """
    if isinstance(index, list):
      gm_weights = []
      gm_means = []
      gm_covs = []
      for k in index:
        weights, means, covs = self.get_single_gm(k)
        gm_weights.append(weights)
        gm_means.append(means)
        gm_covs.append(covs)
    else:
      gm_weights, gm_means, gm_covs = self.get_single_gm(index)
    return gm_weights, gm_means, gm_covs

  def get_single_gm(self, k):
    """Get single Gaussian mixture given index

    Args:
      k: int, the time steps of the Gaussian mixture to fetch.

    Returns:
      weights, means and covariance matrices of the Gaussian mixture requested.
    """
    weights = self.gm_weights[k, :]
    n_gc = np.count_nonzero(weights)
    gm_weights = np.copy(weights[:n_gc])
    gm_means = np.copy(self.gm_means[k, :n_gc, :])
    gm_covs = np.copy(self.gm_covs[k, :n_gc, :, :])
    return gm_weights, gm_means, gm_covs

  def flush(self):
    """Flush the data to disk
    """
    self.file.flush()
