###############################################################################
# Copyright (c) 2018-2019, Tinghui Wang
# License: BSD 3-Clause
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###############################################################################

"""Multi-resident tracking with Global Nearest Neighbor
"""
import numpy as np
from scipy.optimize import linear_sum_assignment
from tensorflow.python.keras.utils.generic_utils import Progbar


def gnn_sg(observations, sensor_graph, adjacency_matrix, max_undetected_period=50):
  """ Nearest neighbor with sensor graph for multi-resident tracking

  Args:
    observations: list of sensor observations, each is a dictionary indexed by
      sensor ID where the value is a dictionary contains details about the
      sensor observation including the time when the sensor becomes "active".
    sensor_graph: A matrix of shape `[num_sensors, num_sensors]`, representing
      the conditional probability of any resident moving from sensor $i$ to
      sensor $j$, where $i$ is the row index, and $j$ is the column index.
    adjacency_matrix: A symmetric matrix of shape `[num_sensors, num_sensors]`,
      representing the adjacency between two sensors. Two sensors are adjacent
      if a resident can trigger both sensor consecutively without activating
      any other sensor in the smart home.
    max_undetected_period: The maximum number of time steps that a target could
      remain undetected by any sensor in the smart home without being assumed to
      have left the house or become inactive.

  Returns:
    List of associations between sensor event and identified targets. Each
    association is a dictionary with the following keys:
    - start: time tag of the sensor event in the sequence
    - sensor: sensor ID
    - tracks: list of track ID. In the case of NN-sg, it only contains one
      element.
  """
  # Temporary variables
  num_targets = 0
  active_targets = set()
  targets_location = {}
  targets_last_active = {}

  # Track ID to sensor observation
  predicted_association = []

  # As GNN-SG takes significant longer time, we use the progress bar from
  # keras to track the progress of the algorithm
  progbar = Progbar(
    # target=len(vec_observations),
    target=len(observations),
    verbose=1,
    stateful_metrics=['cost', 'active targets', 'total targets'],
    unit_name='step'
  )

  # The result holds the association between each sensor event
  # and the tracking targets
  for i, measurements in enumerate(observations):
    # Find previous track
    active_targets_list = list(active_targets)
    num_active_targets = len(active_targets_list)
    measurements_list = list(measurements.keys())
    num_measurements = len(measurements_list)
    # Construct N-by-N log likelihood arrays for Hungarian algorithm
    cost_matrix = np.zeros(
      (num_active_targets, num_measurements), dtype=np.float
    )
    # initialize the cost of the matrix to 10
    cost_matrix[:, :] = 10.
    for t_i, target in enumerate(active_targets_list):
      prev_location = targets_location[target]
      for m_j, sensor in enumerate(measurements_list):
        if adjacency_matrix[prev_location, sensor]:
          if sensor_graph[prev_location, sensor] < 1e-10:
            cost_matrix[t_i, m_j] = 10.
          else:
            cost_matrix[t_i, m_j] = -np.log(
              sensor_graph[prev_location, sensor]
            )
    # Apply Hungarian algorithm (scipy) to solve the linear sum assignment
    row_ind, col_ind = linear_sum_assignment(cost_matrix)
    cost = cost_matrix[row_ind, col_ind].sum()
    # Based on the answer, make associations
    associated_targets = np.zeros((num_measurements,), dtype=np.int)
    for t_i, m_j in zip(row_ind, col_ind):
      target = active_targets_list[t_i]
      prev_location = targets_location[target]
      sensor = measurements_list[m_j]
      if adjacency_matrix[prev_location, sensor]:
        associated_targets[m_j] = target
        targets_last_active[target] = i
        targets_location[target] = sensor
    # For measurements without association, assign new track ID
    for m_j in range(num_measurements):
      if associated_targets[m_j] == 0:
        num_targets += 1
        associated_targets[m_j] = num_targets
        targets_last_active[num_targets] = i
        targets_location[num_targets] = measurements_list[m_j]
        active_targets.add(num_targets)
    predicted_association.append({
      'resident': associated_targets,
      'sensor': measurements_list
    })
    # Targets that has not been detected over certain period of time is considered
    # to have left the house or become inactave. Remove them from the list
    targets_to_remove = []
    for target in active_targets:
      if (i - targets_last_active[target]) >= max_undetected_period:
        targets_to_remove.append(target)
    for target in targets_to_remove:
      active_targets.remove(target)
    progbar.update(i + 1, [
      ('cost', cost),
      ('active targets', len(active_targets)),
      ('total targets', num_targets)
    ])
  # Return
  return predicted_association
