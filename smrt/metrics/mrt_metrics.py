###############################################################################
# Copyright (c) 2018-2019, Tinghui Wang
# License: BSD 3-Clause
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###############################################################################

"""Performance metrics for Multi-resident Tracking in Smart Home

This file implements the following multi-resident tracking metrics:
- association accuracy: The fraction of total sensor events in which the
    ground truth equals the set of predicted resident
- Hamming loss
- Multi-label class classification
"""

import copy
import numpy as np
from sklearn.metrics import hamming_loss
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from tensorflow.python.keras.utils.generic_utils import Progbar

__all__ = [
  'mrt_association_accuracy',
  'mrt_hamming_loss',
  'mrt_classification_report',
  'mrt_truth_prediction_matrix',
  'group_prediction_by_events',
  'group_events_by_track',
  'event_length_by_track',
  'mrta'
]

def mrt_association_accuracy(observations, predicted_association, min_length=3):
  """Association Accuracy

  Association accuracy for multi-resident tracking is defined as the total
  sensor events in which the ground truth equals the set of predicted residents.
  First, we map each predicted track in the `ota` with possible resident
  identifier in the ground truth.
  Secondly, we find the set match based on each sensor event.

  Args:
    observations: Ground truth sensor observations, with detailed information
      about each active sensor, such as start time (`start`), stop time
      (`stop`), and associated residents (`residents`).
    predicted_association: List of two tuple, associating the sensor observation
      with a track identifier.
    min_length: The threshold of sensor events which the track is considered
      valid.

  Returns:
    The association accuracy
  """
  all_residents, truth, prediction = mrt_truth_prediction_matrix(
    observations, predicted_association, min_length
  )
  return accuracy_score(truth, prediction)

def mrt_hamming_loss(observations, predicted_association, min_length=3):
  """Hamming Loss

  Args:
    observations: Ground truth sensor observations, with detailed information
      about each active sensor, such as start time (`start`), stop time
      (`stop`), and associated residents (`residents`).
    predicted_association: List of two tuple, associating the sensor observation
      with a track identifier.
    min_length: The threshold of sensor events which the track is considered
      valid.

  Returns:
    float, the Hamming loss
  """
  all_residents, truth, prediction = mrt_truth_prediction_matrix(
    observations, predicted_association, min_length
  )
  return hamming_loss(truth, prediction)

def mrt_classification_report(observations,
                              predicted_association,
                              min_length=3,
                              **kwargs):
  """MRT Multi-label classification report

  If we treat the resident to sensor event association as a multi-label
  classification problem, this function returns the association summary
  using `classification_report` function provided by `sklearn.metrics`
  package.

  Args:
    observations: Ground truth sensor observations, with detailed information
      about each active sensor, such as start time (`start`), stop time
      (`stop`), and associated residents (`residents`).
    predicted_association: List of two tuple, associating the sensor observation
      with a track identifier.
    min_length: The threshold of sensor events which the track is considered
      valid.
    **kwargs: Additional arguments may include `labels`, `sample_weight`,
      `digits`, `output_dict`, etc. Please see the reference document of
      `sklearn.metrics.classification_report`.

  Returns:
    Text summary of the precision, recall, F1 score for each class, or a
    dictionary if `output_dict` is True.
  """
  all_residents, truth, prediction = mrt_truth_prediction_matrix(
    observations, predicted_association, min_length
  )
  return classification_report(
    y_true=truth, y_pred=prediction, target_names=all_residents,
    **kwargs
  )


def mrt_truth_prediction_matrix(event_list, min_length=3):
  """Acquire confusion matrix for performance calculation using MRT metrics.

  Args:
    observations: Ground truth sensor observations, with detailed information
      about each active sensor, such as start time (`start`), stop time
      (`stop`), and associated residents (`residents`).
    predicted_association: List of two tuple, associating the sensor observation
      with a track identifier.
    min_length: The threshold of sensor events which the track is considered
      valid.

  Returns:
    A three tuple consisting of the resident labels list, ground truth matrix
    and prediction matrix. Both ground truth matrix and prediction matrix are of
    shape `[n_events, n_residents]`.
  """
  events_by_track = group_events_by_track(event_list)
  track_resident_map = associate_track_with_resident(events_by_track)
  track_length = event_length_by_track(events_by_track)
  # Get resident list and reverse index lookup
  all_residents = []
  resident_index_dict = {}
  for event in event_list:
    for resident_label in event['residents']:
      if resident_label not in resident_index_dict:
        resident_index_dict[resident_label] = len(all_residents)
        all_residents.append(resident_label)
  # With all residents, form binary ground truth and prediction matrix
  n_events = len(event_list)
  n_residents = len(all_residents)
  truth = np.zeros((n_events, n_residents), dtype=np.int)
  prediction = np.zeros((n_events, n_residents), dtype=np.int)
  for k, event in enumerate(event_list):
    for resident_label in event['residents']:
      truth[k, resident_index_dict[resident_label]] = 1
    for track in event['tracks']:
      if track_length[track] > min_length:
        resident_label = track_resident_map[track]
        if resident_label is not None:
          prediction[k, resident_index_dict[resident_label]] = 1
  return all_residents, truth, prediction


def mrta_by_nr(event_list,
               min_length=3,
               max_undetected_period=50):
  """Compute MRTA metrics

  MRTA metrics follows the guidelines set by CLEAR MOTA metrics. However, in
  multi-resident tracking applications, a resident can be associated with
  multiple sensor observations and a sensor event can be associated with
  multiple residents. Thus, the CLEAR MOTA metric needs to be adapted to the
  context of multi-resident tracking problem, and here we propose MRTA metrics.

  1. Misses: when a sensor is associated with a resident while in the tracking
    algorithm, there is no track identified that is mapped to that resident, it
    is counted as a miss.
  2. False positives: when a sensor event is associated with a resident and
    there are multiple tracks generated by the tracking algorithm which all
    map to the same resident, it is considered a false positive. Another case
    is that when a track identified by the tracking algorithm is associated with
    a resident that is not linked to the sensor event according to the ground
    truth.
  3. Mismatch: If a resident is still ``active'' according to the ground truth,
    while the track identifier changes in the algorithm output.
  4. Number of Association: The total number of event to resident association
    is the denominator. For example, if a sensor event is associated with two
    resident, the number of ground truth association is 2.

  The MRTA shares the same expression as:
  $1 - \frac{N_{miss} + N_{fp} + N_{mismatch}}{N_{truth}}$
  """
  events_by_track = group_events_by_track(event_list)
  track_resident_map = associate_track_with_resident(events_by_track)
  track_length = event_length_by_track(events_by_track)
  # Temporary Variables
  active_residents = set()
  resident_track_association = {}
  resident_last_active = {}
  # results
  results = {}
  # We use keras progress bar to track the progress
  progbar = Progbar(
    target=len(event_list),
    verbose=1,
    stateful_metrics=['total', 'correct', 'misses', 'fp', 'mismatch'],
    unit_name='step'
  )
  for k, event in enumerate(event_list):
    residents_truth = set(event['residents'])
    num_misses = 0
    num_false_positives = 0
    num_mismatches = 0
    num_correct = 0
    num_associations = len(residents_truth)
    for track in event['tracks']:
      if track_length[track] >= min_length:
        resident = track_resident_map[track]
        if resident is not None:
          if resident in event['residents']:
            if resident not in active_residents:
              # It is correct association
              active_residents.add(resident)
              resident_last_active[resident] = k
              residents_truth.remove(resident)
              resident_track_association[resident] = track
              num_correct += 1
            elif resident_last_active[resident] == k:
              # Means that the resident has been associated with another track
              # identifier, then the current match is a false positive
              num_false_positives += 1
            elif resident_track_association[resident] != track and \
                resident_track_association[resident] != -1 and \
                resident_track_association[resident] not in event['tracks']:
              # It is correct identification, but a mismatch
              resident_last_active[resident] = k
              residents_truth.remove(resident)
              resident_track_association[resident] = track
              num_mismatches += 1
            else:
              # It is correct association
              resident_last_active[resident] = k
              residents_truth.remove(resident)
              resident_track_association[resident] = track
              num_correct += 1
          else:
            # False positive
            num_false_positives += 1
        else:
          num_false_positives += 1
    # After identify mismatches and false positive, check if there are any
    # misses
    num_misses += len(residents_truth)
    # Book keeping
    for resident in residents_truth:
      active_residents.add(resident)
      resident_track_association[resident] = -1
      resident_last_active[resident] = k
    residents_to_deactivate = []
    for resident in active_residents:
      if k - resident_last_active[resident] >= max_undetected_period:
        residents_to_deactivate.append(resident)
    for resident in residents_to_deactivate:
      active_residents.remove(resident)
    num_residents = len(active_residents)
    if num_residents in results:
      results[num_residents]['num_events'] += 1
      results[num_residents]['total'] += num_associations
      results[num_residents]['correct'] += num_correct
      results[num_residents]['misses'] += num_misses
      results[num_residents]['fp'] += num_false_positives
      results[num_residents]['mismatch'] += num_mismatches
    else:
      results[num_residents] = {
        'num_events': 1,
        'total': num_associations,
        'correct': num_correct,
        'misses': num_misses,
        'fp': num_false_positives,
        'mismatch': num_mismatches
      }
    progbar.update(k + 1, [
      ('total', num_associations),
      ('correct', num_correct),
      ('misses', num_misses),
      ('fp', num_false_positives),
      ('mismatch', num_mismatches)
    ])
  return results


def mrta(event_list,
         min_length=3,
         max_undetected_period=50):
  """Compute MRTA metrics

  MRTA metrics follows the guidelines set by CLEAR MOTA metrics. However, in
  multi-resident tracking applications, a resident can be associated with
  multiple sensor observations and a sensor event can be associated with
  multiple residents. Thus, the CLEAR MOTA metric needs to be adapted to the
  context of multi-resident tracking problem, and here we propose MRTA metrics.

  1. Misses: when a sensor is associated with a resident while in the tracking
    algorithm, there is no track identified that is mapped to that resident, it
    is counted as a miss.
  2. False positives: when a sensor event is associated with a resident and
    there are multiple tracks generated by the tracking algorithm which all
    map to the same resident, it is considered a false positive. Another case
    is that when a track identified by the tracking algorithm is associated with
    a resident that is not linked to the sensor event according to the ground
    truth.
  3. Mismatch: If a resident is still ``active'' according to the ground truth,
    while the track identifier changes in the algorithm output.
  4. Number of Association: The total number of event to resident association
    is the denominator. For example, if a sensor event is associated with two
    resident, the number of ground truth association is 2.

  The MRTA shares the same expression as:
  $1 - \frac{N_{miss} + N_{fp} + N_{mismatch}}{N_{truth}}$

  Returns:
    Dictionary containing details of MOTA errors. The dictionary is indexed by
    `total`, `correct`, `miss`, `fp`, `mismatch`.
  """
  events_by_track = group_events_by_track(event_list)
  track_resident_map = associate_track_with_resident(events_by_track)
  track_length = event_length_by_track(events_by_track)
  # Temporary Variables
  active_residents = set()
  resident_track_association = {}
  resident_last_active = {}
  # results
  num_misses = 0
  num_false_positives = 0
  num_mismatches = 0
  num_correct = 0
  num_associations = 0
  # We use keras progress bar to track the progress
  progbar = Progbar(
    target=len(event_list),
    verbose=1,
    stateful_metrics=['total', 'correct', 'misses', 'fp', 'mismatch'],
    unit_name='step'
  )
  for k, event in enumerate(event_list):
    residents_truth = set(event['residents'])
    num_associations += len(residents_truth)
    for track in event['tracks']:
      if track_length[track] >= min_length:
        resident = track_resident_map[track]
        if resident is not None:
          if resident in event['residents']:
            if resident not in active_residents:
              # It is correct association
              active_residents.add(resident)
              resident_last_active[resident] = k
              residents_truth.remove(resident)
              resident_track_association[resident] = track
              num_correct += 1
            elif resident_last_active[resident] == k:
              # Means that the resident has been associated with another track
              # identifier, then the current match is a false positive
              num_false_positives += 1
            elif resident_track_association[resident] != track and \
                resident_track_association[resident] != -1 and \
                resident_track_association[resident] not in event['tracks']:
              # It is correct identification, but a mismatch
              resident_last_active[resident] = k
              residents_truth.remove(resident)
              resident_track_association[resident] = track
              num_mismatches += 1
            else:
              # It is correct association
              resident_last_active[resident] = k
              residents_truth.remove(resident)
              resident_track_association[resident] = track
              num_correct += 1
          else:
            # False positive
            num_false_positives += 1
        else:
          num_false_positives += 1
    # After identify mismatches and false positive, check if there are any
    # misses
    num_misses += len(residents_truth)
    # Book keeping
    for resident in residents_truth:
      active_residents.add(resident)
      resident_track_association[resident] = -1
      resident_last_active[resident] = k
    residents_to_deactivate = []
    for resident in active_residents:
      if k - resident_last_active[resident] >= max_undetected_period:
        residents_to_deactivate.append(resident)
    for resident in residents_to_deactivate:
      active_residents.remove(resident)
    progbar.update(k + 1, [
      ('total', num_associations),
      ('correct', num_correct),
      ('misses', num_misses),
      ('fp', num_false_positives),
      ('mismatch', num_mismatches)
    ])
  print("mrta: %.4f" % (
      1 - (num_misses + num_false_positives + num_mismatches)/num_associations
  ))
  return {
    'total': num_associations,
    'correct': num_correct,
    'miss': num_misses,
    'fp': num_false_positives,
    'mismatch': num_mismatches
  }

def group_prediction_by_events(observations, predicted_association):
  """Group track ID as part of event details

  Args:
    observations: Ground truth sensor observations, with detailed information
      about each active sensor, such as start time (`start`), stop time
      (`stop`), and associated residents (`residents`).
    predicted_association: List of two tuple, associating the sensor observation
      with a track identifier.

  Returns:
    list of sensor events ordered by their activation time. Each sensor event
    is a dictionary with key `start` for activation time, `stop` for
    deactivation time, `sensor` for sensor ID (integer), `residents` for ground
    truth resident labels, and `tracks` for associated track identifiers
    generated by some tracking algorithm.
  """
  active_events = {}
  events_list = []
  n_steps = len(predicted_association)
  for k in range(n_steps):
    obs = observations[k]
    sensors = predicted_association[k]['sensor']
    tracks = predicted_association[k]['resident']
    n_obs = len(sensors)
    for j in range(n_obs):
      sensor = sensors[j]
      # Check existence in the active events
      event_details = obs[sensor]
      # Add or update current event
      if sensor in active_events:
        if active_events[sensor]['start'] == event_details['start'] and \
            active_events[sensor]['stop'] == event_details['stop']:
          # Update prediction list
          if tracks[j] not in active_events[sensor]['tracks']:
            active_events[sensor]['tracks'].append(tracks[j])
        else:
          active_events[sensor] = copy.deepcopy(event_details)
          active_events[sensor]['tracks'] = [tracks[j]]
          active_events[sensor]['sensor'] = sensor
          events_list.append(active_events[sensor])
      else:
        active_events[sensor] = copy.deepcopy(event_details)
        active_events[sensor]['tracks'] = [tracks[j]]
        active_events[sensor]['sensor'] = sensor
        events_list.append(active_events[sensor])
  return events_list

def associate_track_with_resident(events_by_track):
  """Associate each track with resident labeled in the ground truth

  Args:
    events_by_track: A dictionary indexed by track identifier containing a list
      of sensor events associated with the track identifier. Each sensor event
      include a list of ground truth resident labels under `residents`
      key.

  Returns:
    A dictionary indexed by track identifier with the most likely resident
    label.
  """
  track_resident_map = {}
  for r in events_by_track:
    resident_count = {}
    for event_details in events_by_track[r]:
      residents = event_details['residents']
      for resident_label in residents:
        if resident_label in resident_count:
          resident_count[resident_label] += 1
        else:
          resident_count[resident_label] = 1
    # Find the most likely resident
    if len(resident_count) == 0:
      track_resident_map[r] = None
    else:
      resident_label = max(resident_count, key=resident_count.get)
      track_resident_map[r] = resident_label
  return track_resident_map

# def group_events_by_track(observations, predicted_association):
#   """Group sensor events by predicted track ID.
#
#   Args:
#     observations: Ground truth sensor observations, with detailed information
#       about each active sensor, such as start time (`start`), stop time
#       (`stop`), and associated residents (`residents`).
#     predicted_association: List of dictionary indexed by key `resident` and
#       `sensor`. Key `sensor` contains the list of active sensors while key
#       `resident` contains the corresponding track ID.
#
#   Returns:
#     A dictionary indexed by track ID, containing a list of associating sensor
#     events, represented in the form of a two tuple containing sensor ID and
#     event details.
#   """
#   n_steps = len(predicted_association)
#   track_observations = {}
#   track_active_events = {}
#   for k in range(n_steps):
#     n_obs = len(predicted_association[k]['sensor'])
#     for j in range(n_obs):
#       r = predicted_association[k]['resident'][j]
#       sensor = predicted_association[k]['sensor'][j]
#       event_detail = observations[k][sensor]
#       if not track_active_events_contains(
#           track_active_events, r, (sensor, event_detail)
#       ):
#         if r in track_observations:
#           track_observations[r].append((sensor, event_detail))
#         else:
#           track_observations[r] = [(sensor, event_detail)]
#   return track_observations

def group_events_by_track(event_list):
  """Group sensor events by predicted track ID.

  Args:
    event_list: list of sensor events ordered by their activation time.
    Each sensor event is a dictionary with key `start` for activation time,
    `stop` for deactivation time, `sensor` for sensor ID (integer),
    `residents` for ground truth resident labels, and `tracks` for associated
    track identifiers generated by some tracking algorithm.

  Returns:
    Dictionary indexed by track identifiers, each contains a list of sensor
    event associated to the track according to the tracking algorithm.
  """
  n_steps = len(event_list)
  events_by_track = {}
  for k in range(n_steps):
    for track in event_list[k]['tracks']:
      if track in events_by_track:
        events_by_track[track].append(event_list[k])
      else:
        events_by_track[track] = [event_list[k]]
  return events_by_track


def event_length_by_track(track_observations):
  """Computes the length of sensor events associated with each track

  Args:
    track_observations: A dictionary indexed by track ID, containing a list of
      associating sensor events, represented in the form of a two tuple
      containing sensor ID and event details.

  Returns:
    A dictionary indexed by track ID, containing the length of associated
    sensor events
  """
  track_length = {}
  for r in track_observations:
    track_length[r] = len(track_observations[r])
  return track_length

def track_active_events_contains(track_active_events, r, obs):
  """Check if current observation is contained in the "active events" list of
  tracks. If the current observation does not exists, add it to active events
  of the corresponding sensors. This function also check the time of active
  events and remove any expired sensor events from the list by looking at the
  start time and stop time of the event.

  Args:
    track_active_events: A dictionary indexed by track identifier `r`, contains
      a list of sensor events.
    r: Track identifier.
    obs: Two tuple composed of the sensor ID and the detail of the sensor event.

  Returns:
    True, if current observation exists in the current active events. False,
    otherwise.
  """
  sensor_id = obs[0]
  event_details = obs[1]
  # Handles the case where the track ID does not exist in the
  # `track_active_events`
  if r not in track_active_events:
    track_active_events[r] = {}
    track_active_events[r][sensor_id] = event_details
    return False
  # Now check the list
  active_events = track_active_events[r]
  # Remove expired events
  sensors_to_remove = []
  for sensor_id in active_events:
    if active_events[sensor_id]['stop'] is None or \
        event_details['start'] is None:
      # print("Type error occurs when comparing active events with event details",
      #       "\n\rActive Events:", sensor_id, active_events[sensor_id],
      #       "\n\rEvent Details:", event_details)
      continue
    if active_events[sensor_id]['stop'] < event_details['start']:
      sensors_to_remove.append(sensor_id)
  for sensor in sensors_to_remove:
    active_events.pop(sensor)
  if sensor_id in active_events:
    # Compare sensor events
    if active_events[sensor_id]['start'] == event_details['start'] and \
        active_events[sensor_id]['stop'] == event_details['stop']:
      return True
  active_events[sensor_id] = event_details
  return False
