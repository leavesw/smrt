"""Depricated"""
import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp
import tensorflow.python.keras as keras


class LinearMultiTargetGaussianMixtureModel(keras.Model):
  """Linear Multi-target model in Gaussian Mixture

  Args:
      n_visible: visible dims
      n_hidden: hidden dims
      n_birth: number of birth component in the model
  """
  def __init__(self,
               n_visible,
               n_hidden,
               n_birth):
    super(LinearMultiTargetGaussianMixtureModel, self).__init__()
    self.ndims = n_visible + n_hidden
    self.n_visible = n_visible
    self.n_hidden = n_hidden
    self.n_birth = n_birth
    # detection rate
    self.p_d = tf.Variable(initial_value=1., name="p_d")
    # survival rate
    self.p_s = tf.Variable(initial_value=1., name="p_s")
    # Dynamic Model Linear Multiplier
    self.F = tf.Variable(
      initial_value=np.ones((self.ndims, self.ndims), dtype=np.float),
      name="D_Mul"
    )
    # Dynamic Model Gaussian Covariance
    self.Q = tf.Variable(
      initial_value=np.eye(self.ndims, dtype=np.float),
      name="D_Cov"
    )
    # Measurement Model Multiplier, fixed - only shows the visible part.
    self.H = tf.constant(
      np.block([
        np.eye(self.n_visible, dtype=np.float),
        np.zeros((self.n_visible, self.n_hidden), dtype=np.float)
      ]),
      name="M_Mul"
    )
    # Observation Gaussian Error
    self.R = tf.Variable(
      initial_value=np.eye (self.n_visible, dtype=np.float),
      name="M_Cov"
    )
    # Birth GM-PHD
    self.birth_weights = tf.Variable(
      initial_value=np.full(
        shape=(self.n_birth,), fill_value=1./self.n_birth, dtype=np.float
      ),
      name="Birth_weights"
    )
    self.birth_means = tf.Variable(
      initial_value=np.random.uniform(
        low=-1., high=1., size=(self.n_birth, self.n_visible)
      ),
      name="Birth_means"
    )
    self.birth_covs = tf.Variable(
      initial_value=np.stack([
        np.eye(self.n_visible, dtype=np.float) * (1./self.n_birth ** 2)
        for i in range(self.n_birth)
      ],
      name="birth_covs")
    )
    # Set Clutter parameter as Point Poisson Process with uniform spatial
    # distribution
    self.clutter_lam = tf.Variable(initial_value=1., name="lam_c")
    self.clutter_z = tf.Variable(initial_value=0.05, name="c_z")

  def call(self, inputs, training=False, mask=None):
    """Implement Linear Multi-target Gaussian Model forward pass.

    During the training phase, the inputs should be composed of the Gaussian
    mixture of the multi-target PHD of the predictor of current time step $k$,
    and the observation at current time step $k$. The forward pass corrects
    the multi-target PHD of the predictor based on the current observation,
    and predict the PHD of the next time step $k+1$.

    During the evaluation phase, the inputs should be composed of the Gaussian
    mixture of the multi-target PHD after corrector at previous time step $k-1$.
    The forward pass calculated the predicted

    Arguments:
      inputs: A list of tensors encodes the weights, means and covariance
        matrix of PHD in the form of a Gaussian mixture and the observations at
        current time step.
      training: Boolean or boolean scalar tensor, indicating whether to run
        the model in training mode or inference mode.
      mask: A mask or list of masks. A mask can be either a tensor or None
        (no mask). Ignored at the moment.

    Returns:

    """
    # Acquire the variables for GM-PHD of previous time step
    gm_weights = inputs[0]
    gm_means = inputs[1]
    gm_covs = inputs[2]
    # Get set of observations
    observations = inputs[3]

  def gm_predictor(self, gm_weights, gm_means, gm_covs):
    """GM-PHD Predictor

    The GM predictor processes the list of GMs at time k based on dynamic model.
    """
    # Weights are decayed by target persistence probability $p_s$.
    predictor_weights = gm_weights * self.p_s
    predictor_means = tf.matmul(gm_means, self.F, transpose_b=True)
    # In order to broadcast F with multiple GM covariance matrices, F needs to
    # be broadcasted into same dimensions as gm_covs
    F_padded = tf.broadcast_to(
      self.F, tf.concat([gm_covs.shape[:-2], self.F.shape], axis=0)
    )
    F_T = tf.transpose(self.F)
    F_T_padded = tf.broadcast_to(
      F_T, tf.concat([gm_covs.shape[:-2], F_T.shape], axis=0)
    )
    predictor_covs = tf.matmul(F_padded, tf.matmul(gm_covs, F_T_padded))
    return predictor_weights, predictor_means, predictor_covs

  def gm_append_birth(self, gm_weights, gm_means, gm_covs):
    """GM-PHD Append birth PHD to existing PHD
    """
    new_weights = tf.concat([gm_weights, self.birth_weights], axis=0)
    new_means = tf.concat([gm_means, self.birth_means], axis=0)
    new_covs = tf.concat([gm_covs, self.birth_covs], axis=0)
    return new_weights, new_means, new_covs

  def gm_corrector(self, gm_weights, gm_means, gm_covs, observations):
    """GM-PHD Corrector
    """
    # The first part of the posterior PHD is composed of un-detected targets.
    undetected_weights = gm_weights - self.p_d * gm_weights
    undetected_means = gm_means
    undetected_covs = gm_covs
    # The second part is composed of GMs corrected by current observation
    # Get the Gaussian distribution for $q_j(z)$, where $j$ is the index into
    # the number of Gaussian components in the input PHD.
    # Here are the shapes of all tensors:
    # - `q_means`: `[num_gc, n_visible]`
    # - `q_covs`: `[num_gc, n_visible, n_visible]`
    q_means = tf.matmul(gm_means, self.H, transpose_b=True)
    # H_padded shape: `[num_gc, n_visible, ndims]`
    H_padded = tf.broadcast_to(
      self.H, tf.concat([gm_covs.shape[:-2], self.H.shape], axis=0)
    )
    H_T = tf.transpose(self.H)
    # H_padded shape: `[num_gc, ndims, n_visible]`
    H_T_padded = tf.broadcast_to(
      H_T, tf.concat([gm_covs.shape[:-2], H_T.shape], axis=0)
    )
    q_covs = self.R + tf.matmul(H_padded, tf.matmul(gm_covs, H_T_padded))
    # Compute K of shape `[num_gc, ndims, n_visible]`
    K = tf.matmul(gm_covs, tf.matmul(H_T_padded, tf.linalg.inv(q_covs)))
    # Compute q array, of shape `[num_z, num_gc]`
    q_dists = tfp.distributions.MultivariateNormalFullCovariance(
      loc=q_means, covariance_matrix=q_covs, name="q_dists"
    )
    obs_broadcasted = tf.transpose(tf.broadcast_to(
      observations, tf.concat([[q_means.shape[0]], observations.shape], axis=0)
    ), axis=[1, 0, 2])
    q = q_dists.prob(obs_broadcasted, name="q_z_j")
    # Compute new weights
    # wq of shape `[num_z, num_gc]`
    wq = tf.reshape(gm_weights, tf.concat([gm_weights.shape, [1]], axis=0)) * q
    corrected_weights_numerator = (self.p_d * wq)
    corrected_weights_denominator = (self.clutter_lam * self.clutter_z +
      self.p_d * tf.reduce_sum(wq, axis=1)
    )
    corrected_weights = corrected_weights_numerator / tf.reshape(
      corrected_weights_denominator,
      tf.concat([corrected_weights_denominator.shape, [1]], axis=0)
    )
    # Compute corrected mean, of shape `[num_z, num_gc, ndims]`
    corrected_mean = gm_means + tf.matmul(
      K, (observations - tf.matmul(self.H, gm_means, ))
    )



