###############################################################################
# Copyright (c) 2018-2019, Tinghui Wang
# License: BSD 3-Clause
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###############################################################################
import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp
from ._helper import tf_check_covs, tf_check_symmetric


class TfLinearMTGM:
  """A trainable linear multi-target Gaussian Mixture model implemented using
  tensorflow as backend.

  Args:
      n_visible: visible dims
      n_hidden: hidden dims
      n_birth: number of birth component in the model
  """
  def __init__(self,
               n_visible,
               n_hidden,
               n_birth):
    self.ndims = n_visible + n_hidden
    self.n_visible = n_visible
    self.n_hidden = n_hidden
    self.n_birth = n_birth
    # detection rate
    self.p_d_logit = tf.Variable(
      initial_value=1., name="p_d_logit", dtype=tf.float64
    )
    # survival rate
    self.p_s_logit = tf.Variable(
      initial_value=1., name="p_s_logit", dtype=tf.float64
    )
    # Dynamic Model Linear Multiplier
    self.F = tf.Variable(
      initial_value=np.ones((self.ndims, self.ndims), dtype=np.float),
      dtype=tf.float64,
      name="D_Mul"
    )
    # Dynamic Model Gaussian Covariance
    self.Q_dev = tf.Variable(
      initial_value=np.eye(self.ndims, dtype=np.float),
      dtype=tf.float64,
      name="D_Cov"
    )
    # Measurement Model Multiplier, fixed - only shows the visible part.
    self.H = tf.constant(
      np.block([
        np.eye(self.n_visible, dtype=np.float),
        np.zeros((self.n_visible, self.n_hidden), dtype=np.float)
      ]),
      dtype=tf.float64,
      name="M_Mul"
    )
    # Observation Gaussian Error
    self.R_dev = tf.Variable(
      initial_value=np.eye(self.n_visible, dtype=np.float),
      dtype=tf.float64,
      name="M_Cov"
    )
    # Birth GM-PHD
    self.birth_weights = tf.Variable(
      initial_value=np.full(
        shape=(self.n_birth,), fill_value=1./self.n_birth, dtype=np.float
      ),
      dtype=tf.float64,
      name="Birth_weights"
    )
    self.birth_means = tf.Variable(
      initial_value=np.random.uniform(
        low=-1., high=1., size=(self.n_birth, self.ndims)
      ),
      dtype=tf.float64,
      name="Birth_means"
    )
    self.birth_covs_dev = tf.Variable(
      initial_value=np.stack([
        np.eye(self.ndims, dtype=np.float) * (1./self.n_birth ** 2)
        for i in range(self.n_birth)
      ]),
      dtype=tf.float64,
      name="birth_covs"
    )
    # Set Clutter parameter as Point Poisson Process with uniform spatial
    # distribution
    self.clutter_lam = tf.Variable(
      initial_value=1., name="lam_c", dtype=tf.float64
    )
    self.clutter_z = tf.Variable(
      initial_value=0.05, name="c_z", dtype=tf.float64
    )
    # Define trainable variable for fit
    self.trainable_variables = [
      self.p_d_logit, self.p_s_logit, self.F, self.Q_dev, self.R_dev, self.birth_weights,
      self.birth_means, self.birth_covs_dev, self.clutter_lam, self.clutter_z
    ]
    # Define the parameters that are weights for callbacks
    self.weights = [
      self.p_d_logit, self.p_s_logit, self.F, self.Q_dev, self.R_dev,
      self.birth_weights, self.birth_means, self.birth_covs_dev,
      self.clutter_lam, self.clutter_z
    ]
    self.Jmax = 100

  @tf.function
  def fit(self, inputs, observations, optimizer, loss):
    """Fit the model based on inputs with lists of observations
    """
    with tf.GradientTape() as t:
      outputs = self.call(inputs)
      # tf_weights, tf_means, tf_covs = outputs
      # np_loss = np_observations_loglikelihood(
      #   (tf_weights.numpy(), tf_means.numpy(), tf_covs.numpy()),
      #   observations
      # )
      # print("np_loss: ", np_loss)
      loss_value = loss(outputs, observations)
    grads = t.gradient(loss_value, self.trainable_variables)
    optimizer.apply_gradients(zip(grads, self.trainable_variables))
    return loss_value, outputs

  def call(self, inputs):
    """Implement Linear Multi-target Gaussian Model forward pass.

    During the training phase, the inputs should be composed of the Gaussian
    mixture of the multi-target PHD of the predictor of current time step $k$,
    and the observation at current time step $k$. The forward pass corrects
    the multi-target PHD of the predictor based on the current observation,
    and predict the PHD of the next time step $k+1$.

    During the evaluation phase, the inputs should be composed of the Gaussian
    mixture of the multi-target PHD after corrector at previous time step $k-1$.
    The forward pass calculated the predicted

    Arguments:
      inputs: A list of tensors encodes the weights, means and covariance
        matrix of PHD in the form of a Gaussian mixture and the observations at
        current time step
      training: Boolean or boolean scalar tensor, indicating whether to run
        the model in training mode or inference mode.
      mask: A mask or list of masks. A mask can be either a tensor or None
        (no mask). Ignored at the moment.

    Returns:

    """
    # Acquire the variables for GM-PHD of previous time step
    gm_weights = inputs[0]
    gm_means = inputs[1]
    gm_covs = inputs[2]
    # Get set of observations
    observations = inputs[3]
    # print("gm_weights Shape: ", gm_weights.shape)
    # print("gm_means Shape: ", gm_means.shape)
    # print("gm_covs Shape: ", gm_covs.shape)
    # print("observations Shape: ", observations.shape)
    # Append birth
    gm_weights, gm_means, gm_covs = self.gm_append_birth(
      gm_weights, gm_means, gm_covs
    )
    gm_weights, gm_means, gm_covs = self.gm_corrector(
      gm_weights, gm_means, gm_covs, observations
    )
    gm_weights, gm_means, gm_covs = self.gm_predictor(
      gm_weights, gm_means, gm_covs
    )
    return self.gm_truncate(gm_weights, gm_means, gm_covs, self.Jmax)

  def gm_truncate(self, gm_weights, gm_means, gm_covs, Jmax):
    """Truncate gaussian mixtures
    """
    sorted_indices = tf.argsort(gm_weights, direction="DESCENDING")
    gather_indices = tf.expand_dims(sorted_indices[:Jmax], axis=1)
    return tf.gather_nd(gm_weights, gather_indices), \
      tf.gather_nd(gm_means, gather_indices), \
      tf.gather_nd(gm_covs, gather_indices)

  def gm_predictor(self, gm_weights, gm_means, gm_covs):
    """GM-PHD Predictor

    The GM predictor processes the list of GMs at time k based on dynamic model.
    """
    # Weights are decayed by target persistence probability $p_s$.
    p_s = tf.nn.sigmoid(self.p_s_logit)
    predictor_weights = gm_weights * p_s
    predictor_means = tf.matmul(gm_means, self.F, transpose_b=True)
    # In order to broadcast F with multiple GM covariance matrices, F needs to
    # be broadcasted into same dimensions as gm_covs
    F_padded = tf.broadcast_to(
      self.F, tf.concat([tf.shape(gm_covs)[:-2], tf.shape(self.F)], axis=0)
    )
    # TODO: Remove debugger codes
    gm_covs_shape = tf.shape(gm_covs)
    for i in range(gm_covs_shape[0]):
      try:
        chol_matrix = tf.linalg.cholesky(gm_covs[i, :, :])
      except:
        print("Error Founded at ", i)
    # debug end
    gm_chol_covs = tf.linalg.cholesky(gm_covs)
    F_cov_chols = tf.matmul(F_padded, gm_chol_covs)
    Q = tf.matmul(self.Q_dev, tf.transpose(self.Q_dev))
    predictor_covs = tf.matmul(
      F_cov_chols, tf.transpose(F_cov_chols, perm=[0, 2, 1])
    ) + Q
    # TODO: Remove codes for debug
    # Check if the covariance are symmetric
    # non_sym = tf_check_symmetric(predictor_covs)
    # if len(non_sym) > 0:
    #   print("Non symmetric matrices found.")
    return predictor_weights, predictor_means, predictor_covs

  def gm_append_birth(self, gm_weights, gm_means, gm_covs):
    """GM-PHD Append birth PHD to existing PHD
    """
    new_weights = tf.concat([gm_weights, self.birth_weights], axis=0)
    new_means = tf.concat([gm_means, self.birth_means], axis=0)
    birth_covs = tf.matmul(
      self.birth_covs_dev, tf.transpose(self.birth_covs_dev, perm=[0, 2, 1])
    )
    new_covs = tf.concat([gm_covs, birth_covs], axis=0)
    return new_weights, new_means, new_covs

  def gm_corrector(self, gm_weights, gm_means, gm_covs, observations):
    """GM-PHD Corrector
    """
    # The first part of the posterior PHD is composed of un-detected targets.
    p_d = tf.nn.sigmoid(self.p_d_logit)
    undetected_weights = gm_weights - p_d * gm_weights
    undetected_means = gm_means
    undetected_covs = gm_covs
    # Corrected GMs
    hm = self._corrector_compute_hm(gm_means=gm_means)
    r_hph = self._corrector_compute_r_hph(gm_covs=gm_covs)
    K = self._corrector_compute_k(gm_covs=gm_covs, r_hph=r_hph)
    qz = self._corrector_compute_qz(
      observations=observations, hm=hm, r_hph=r_hph
    )
    wq = self._corrector_compute_wq(gm_weights=gm_weights, qz=qz)
    w_den = self._corrector_compute_weights_denominator(wq=wq)
    corrected_weights = tf.reshape(self._corrector_compute_weights(
      w_den=w_den, wq=wq
    ), shape=[-1])
    corrected_means = tf.reshape(self._corrector_compute_means(
      gm_means=gm_means, observations=observations, K=K, hm=hm
    ), shape=[-1, self.ndims])
    corrected_covs = tf.reshape(self._corrector_compute_covs(
      gm_covs=gm_covs, K=K, n_obs=tf.shape(observations)[0]
    ), shape=[-1, self.ndims, self.ndims])
    return tf.concat([undetected_weights, corrected_weights], axis=0), \
      tf.concat([undetected_means, corrected_means], axis=0), \
      tf.concat([undetected_covs, corrected_covs], axis=0)

  def _corrector_compute_weights_denominator(self, wq):
    """Compute the denominator of weights in the posterior PHD after the GM-PHD
    corrector.

    Args:
      wq: The $w^{(j)}q^{(j)}(z) term in the GM-PHD corrector weight
        calculation, of shape `[num_obs, num_gc]`.

    Returns:
      The denominator of weights in the posterior PHD after the GM-PHD
      corrector, of shape `[num_obs,]`.
    """
    p_d = tf.nn.sigmoid(self.p_d_logit)
    w_den = self.clutter_lam * self.clutter_z + p_d * tf.reduce_sum(
      wq, axis=1
    )
    return w_den

  def _corrector_compute_weights(self, w_den, wq):
    """Compute the weights of the Gaussian components in the posterior PHD after
    the corrector

    Args:
      w_den: denominator of the weights, of shape `[num_obs,]`.
      wq: The $w^{(j)}q^{(j)}(z) term in the GM-PHD corrector weight
        calculation, of shape `[num_obs, num_gc]`.

    Returns:
      the weights of the Gaussian components in the posterior PHD after
      the corrector, of shape `[num_obs, num_gc]`.
    """
    # w_den_padded: `[num_obs, num_gc]`
    w_den_padded = tf.transpose(tf.broadcast_to(
      w_den, shape=tf.concat([tf.shape(wq)[1:], tf.shape(w_den)], axis=0)
    ))
    return tf.divide(wq, w_den_padded)

  def _corrector_compute_wq(self, gm_weights, qz):
    """Compute the $w^{(j)}q^{(j)}(z) term in the GM-PHD corrector
    weight calculation.

    Args:
      gm_weights: The weights of Gaussian components in the prior PHD, of shape
        `[num_gc,]`.
      qz: The $q^{(j)}(z)$ term in the GM-PHD corrector, of shape
        `[num_obs, num_gc]`

    Returns:
      the $w^{(j)}q^{(j)}(z) term in the GM-PHD corrector weight calculation,
      of shape `[num_obs, num_gc]`.
    """
    # weights_padded: `[num_obs, num_gc]`
    weights_padded = tf.broadcast_to(
      gm_weights, shape=tf.concat(
        [tf.shape(qz)[:-1], tf.shape(gm_weights)],
        axis=0
      )
    )
    wq = tf.multiply(weights_padded, qz)
    return wq

  def _corrector_compute_qz(self, observations, hm, r_hph):
    """Compute the $q^{(j)}(z)$ term in the GM-PHD corrector.

    :math:`q_k^{(j)}(z) = \mathcal{N}(z; Hm^{(j)}, R+HP^{(j)}H^T)`

    Args:
      observations: Sensor observations of the targets, of shape
        `[num_obs, n_visible}`.
      hm: Term Hm in the GM-PHD corrector, of shape `[num_gc, n_visible]`.
      r_hph: (R + HPH^T) term of shape `[num_gc, n_visible, n_visible]`.

    Returns:
      The $q^{(j)}(z)$ term in the GM-PHD corrector, of shape
      `[num_obs, num_gc]`
    """
    q_dists = tfp.distributions.MultivariateNormalFullCovariance(
      loc=hm, covariance_matrix=r_hph, name="q_dists"
    )
    obs_padded = tf.transpose(
      tf.broadcast_to(
        observations,
        shape=tf.concat([tf.shape(hm)[:-1], tf.shape(observations)], axis=0)
      ), perm=[1, 0, 2]
    )
    qz = q_dists.prob(obs_padded, name="q_z")
    return qz

  def _corrector_compute_means(self, gm_means, observations, K, hm):
    """Compute the means of the Gaussian components in the posterior
    GM-PHD after the corrector.

    :math:`m_k^{(j)}(z) = m^{(j)} + K(z - Hm^{(j)})`

    Args:
      gm_means: Mean vectors for Gaussian components, of shape
        `[num_gc, ndims]`.
      observations: Sensor observations of the targets, of shape
        `[num_obs, n_visible}`.
      K: Term K in GM-PHD corrector, of shape `[num_gc, ndims, n_visible]`
      hm: Term Hm in the GM-PHD corrector, of shape `[num_gc, n_visible]`.

    Returns:
      The means of the Gaussian components in the posterior GM-PHD after
      the corrector, of shape `[num_obs, num_gc, ndims]`.
    """
    # hm_padded: `[num_obs, num_gc, n_visible]`
    hm_padded = tf.broadcast_to(
      hm, shape=tf.concat([tf.shape(observations)[:-1], tf.shape(hm)], axis=0)
    )
    # obs_padded: `[num_obs, num_gc, n_visible]`
    obs_padded = tf.transpose(
      tf.broadcast_to(
        observations,
        shape=tf.concat([tf.shape(gm_means)[:-1], tf.shape(observations)],
                        axis=0)
      ), perm=[1, 0, 2]
    )
    # z_hm = obs_padded - hm_padded, of shape `[num_obs, num_gc, n_visible, 1]`
    z_hm = tf.expand_dims(obs_padded - hm_padded, axis=-1)
    # means_padded: `[num_obs, num_gc, ndims, 1]`
    means_padded = tf.expand_dims(tf.broadcast_to(
      gm_means, shape=tf.concat([tf.shape(observations)[:-1],
                                 tf.shape(gm_means)], axis=0)
    ), axis=-1)
    # K_padded: `[num_obs, num_gc, n_dims, n_visible]`
    K_padded = tf.broadcast_to(
      K, shape=tf.concat([tf.shape(observations)[:-1], tf.shape(K)], axis=0)
    )
    # means: `[num_obs, num_gc, n_dims, 1]`
    means = means_padded + tf.matmul(K_padded, z_hm)
    means = tf.reshape(means, shape=tf.shape(means)[:-1])
    return means

  def _corrector_compute_covs(self, gm_covs, K, n_obs):
    """Compute the covariance matrices for the Gaussian components in the
    posterior GM-PHD after the corrector

    :math:`P_k^{(j)} = [I - K^{(j)} H] P^{j}

    Args:
      gm_covs: Covariance matrices of Gaussian components, of shape
        `[num_gc, ndims, ndims]`.
      K: Term K in GM-PHD corrector, of shape `[num_gc, ndims, n_visible]`
      n_obs: Number of observations at current time step.

    Returns:
      Corrected covariance matrics of shape `[num_obs, num_gc, ndims, ndims]`
    """
    unity_padded = tf.eye(
      self.ndims, batch_shape=(tf.shape(gm_covs)[0],), dtype=tf.float64
    )
    # H_padded shape: `[num_gc, n_visible, ndims]`
    H_padded = tf.broadcast_to(
      self.H, tf.concat([tf.shape(gm_covs)[:-2], self.H.shape], axis=0)
    )
    # Before submitting the covariances, we insert a piece of debugger code
    # here to check whether the covariance derived is positive determinant.
    # Check the multiplier for gm_covs is positive definite
    covs_multiplier = unity_padded - tf.matmul(K, H_padded)
    # covs_multiplier = tf_check_covs(covs_multiplier, tol=1e-4)
    covs = tf.matmul(
      covs_multiplier, gm_covs
    )
    # TODO: Remove codes for debug
    # Check if the covariance are symmetric
    # non_sym = tf_check_symmetric(covs)
    # if len(non_sym) > 0:
    #   print("Non symmetric matrices found.")
    # End of debugging codes
    # Enforce symmetry
    covs = 0.5 * (covs + tf.transpose(
      covs, perm=[0, 2, 1]
    ))
    # Enforce positive definite
    # covs = tf_check_covs(covs)
    covs_padded = tf.broadcast_to(
      covs, shape=tf.concat([[n_obs], tf.shape(covs)], axis=0)
    )
    return covs_padded

  def _corrector_compute_hm(self, gm_means):
    """Compute Hm term in the GM-PHD corrector

    :math:`Hm^{j}`

    Args:
      gm_means: Mean vectors for Gaussian components, of shape
        `[num_gc, ndims]`

    Returns:
      Hm term in the GM-PHD corrector, of shape `[num_gc, n_visible]`.
    """
    hm = tf.matmul(gm_means, self.H, transpose_b=True, name="hm")
    return hm

  def _corrector_compute_k(self, gm_covs, r_hph):
    """Compute term K in the GM-PHD corrector

    :math:`K^{(j)}=P^{(j)}H^T(HP^{(j)}H^T + R)^{-1}`

    Args:
      gm_covs: Covariance matrices of Gaussian components, of shape
        `[num_gc, ndims, ndims]`.
      r_hph: (R + HPH^T) term of shape `[num_gc, n_visible, n_visible]`.

    Returns:
      Term $K$ in GM-PHD corrector, of shape `[num_gc, ndims, n_visible]`.
    """
    H_T = tf.transpose(self.H)
    # H_padded shape: `[num_gc, ndims, n_visible]`
    H_T_padded = tf.broadcast_to(
      H_T, tf.concat([tf.shape(gm_covs)[:-2], H_T.shape], axis=0)
    )
    # In order to guarantee the inverse is positive definite, do
    # Cholesky decomposition, inverse, and then compute K.
    r_hph_chol = tf.linalg.cholesky(r_hph)
    r_hph_chol_inv = tf.linalg.inv(r_hph_chol)
    r_hph_inv = tf.matmul(tf.transpose(r_hph_chol_inv, perm=[0, 2, 1]),
                          r_hph_chol_inv)
    K = tf.matmul(gm_covs, tf.matmul(H_T_padded, r_hph_inv))
    return K

  def _corrector_compute_r_hph(self, gm_covs):
    """Compute (R + HPH^T) term in the GM-PHD corrector

    :math:`H_k P_{k|k-1}^{(j)}H_k^T + R_{k}`

    Args:
      gm_covs: Covariance matrices of Gaussian components, of shape
        `[num_gc, ndims, ndims]`.

    Returns:
      (R + HPH^T) calculated based on each Gaussian components, of shape
      `[num_gc, n_visible, n_visible]`.
    """
    R = tf.matmul(self.R_dev, tf.transpose(self.R_dev))
    R_padded = tf.broadcast_to(
      R, tf.concat([tf.shape(gm_covs)[:-2], R.shape], axis=0),
      name="R_padded"
    )
    # H_padded shape: `[num_gc, n_visible, ndims]`
    H_padded = tf.broadcast_to(
      self.H, tf.concat([tf.shape(gm_covs)[:-2], self.H.shape], axis=0)
    )
    H_T = tf.transpose(self.H)
    # H_padded shape: `[num_gc, ndims, n_visible]`
    H_T_padded = tf.broadcast_to(
      H_T, tf.concat([tf.shape(gm_covs)[:-2], H_T.shape], axis=0)
    )
    r_hph = R_padded + tf.matmul(H_padded, tf.matmul(gm_covs, H_T_padded))
    # TODO: Remove codes for debug
    # Check if the covariance are symmetric
    # non_sym = tf_check_symmetric(r_hph)
    # if len(non_sym) > 0:
    #   print("R+HPH': Non symmetric matrices found.")
    return r_hph


class TfLinearMTGM_v2:
  """A trainable linear multi-target Gaussian Mixture model implemented using
  tensorflow as backend.

  Version 2 is an attempt to solve positive determinant error during training
  and propagation such that Cholesky decomposition may be unsuccessful. The
  attempt used in this implementation is to propagate the cholesky decomposition
  triangle matrix instead of the full covariance matrix during the learning
  and filtering process.

  Args:
    n_visible: visible dims
    n_hidden: hidden dims
    n_birth: number of birth component in the model
  """
  def __init__(self,
               n_visible,
               n_hidden,
               n_birth):
    self.ndims = n_visible + n_hidden
    self.n_visible = n_visible
    self.n_hidden = n_hidden
    self.n_birth = n_birth
    # detection rate
    self.p_d_logit = tf.Variable(
      initial_value=1., name="p_d_logit", dtype=tf.float64
    )
    # survival rate
    self.p_s_logit = tf.Variable(
      initial_value=1., name="p_s_logit", dtype=tf.float64
    )
    # Dynamic Model Linear Multiplier
    self.F = tf.Variable(
      initial_value=np.block([[
        np.eye(n_visible, dtype=np.float),
        np.eye(n_visible, n_hidden, dtype=np.float)
      ],[
        np.zeros((n_hidden, n_visible), dtype=np.float),
        np.eye(n_hidden, dtype=np.float)
      ]]),
      dtype=tf.float64,
      trainable=False,
      name="F"
    )
    # Dynamic Model Gaussian Covariance Cholesky decomposed (Lower Triangle)
    self.Q_chol_var = tf.Variable(
      initial_value=0.01 * np.eye(self.ndims, dtype=np.float),
      dtype=tf.float64,
      name="Q_chol_var"
    )
    # Measurement Model Multiplier, fixed - only shows the visible part.
    self.H = tf.constant(
      np.block([
        np.eye(self.n_visible, dtype=np.float),
        np.zeros((self.n_visible, self.n_hidden), dtype=np.float)
      ]),
      dtype=tf.float64,
      name="H"
    )
    # Observation Gaussian Error
    self.R_chol_var = tf.Variable(
      initial_value=0.01 * np.eye(self.n_visible, dtype=np.float),
      dtype=tf.float64,
      name="R_chol_var"
    )
    # Birth GM-PHD
    self.birth_weights = tf.Variable(
      initial_value=np.full(
        shape=(self.n_birth,), fill_value=1./self.n_birth, dtype=np.float
      ),
      dtype=tf.float64,
      name="Birth_weights"
    )
    self.birth_means = tf.Variable(
      initial_value=np.random.uniform(
        low=-1., high=1., size=(self.n_birth, self.ndims)
      ),
      dtype=tf.float64,
      name="Birth_means"
    )
    self.birth_covs_chol_vars = tf.Variable(
      initial_value=np.stack([
        np.eye(self.ndims, dtype=np.float) * (1./self.n_birth ** 2)
        for i in range(self.n_birth)
      ]),
      dtype=tf.float64,
      name="birth_covs_chol_vars"
    )
    # Set Clutter parameter as Point Poisson Process with uniform spatial
    # distribution
    self.clutter_lam = tf.Variable(
      initial_value=1., name="lam_c", dtype=tf.float64
    )
    self.clutter_z = tf.Variable(
      initial_value=0.05, name="c_z", dtype=tf.float64
    )
    # Define variable categories
    self.variables_by_cat = {
      'variance': [self.Q_chol_var, self.R_chol_var],
      'multiplier': [self.F],
      'scalar': [self.p_d_logit, self.p_s_logit],
      'p_d': [self.p_d_logit],
      'p_s': [self.p_s_logit],
      'clutter': [self.clutter_lam, self.clutter_z],
      'birth': [self.birth_weights, self.birth_means, self.birth_covs_chol_vars]
    }
    # Define trainable variable for fit
    self.trainable_variables = [
      self.p_d_logit, self.p_s_logit, # self.F,
      self.Q_chol_var, self.R_chol_var,
      self.birth_weights, self.birth_means, self.birth_covs_chol_vars,
      self.clutter_lam, self.clutter_z
    ]
    # Define the parameters that are weights for callbacks
    self.weights = [
      self.p_d_logit, self.p_s_logit,
      self.Q_chol_var, self.R_chol_var,
      self.birth_weights, self.birth_means, self.birth_covs_chol_vars,
      self.clutter_lam, self.clutter_z
    ]
    # Maximum number of Gaussian components to keep
    self.Jmax = 100

  def getTrainableVariables(self):
    """Get a list of trainable variables

    Returns:
      list of TF Variables, each is marked as trainable
    """
    return self.trainable_variables

  def setVariablesTraibilityByCategory(self, category, isTrainable):
    """Set a categories of variables trainable or not.

    By default, all variables are marked trainable in the program, and added to
    trainable variables list: self.trainable_variables. However, their
    trainability can be disabled by categories.

    Args:
      category: str, name of the category. Valid values are `variance`,
        `multiplier`, `birth`, `scalar` or `clutter`.
      isTrainable: bool, True if trainable, False otherwise.

    Returns:
      list of TF Variables, each is marked as trainable in the model
    """
    if isTrainable:
      for v in self.variables_by_cat[category]:
        if v not in self.trainable_variables:
          self.trainable_variables.append(v)
    else:
      for v in self.variables_by_cat[category]:
        if v in self.trainable_variables:
          self.trainable_variables.remove(v)
    return self.trainable_variables

  def fit(self, inputs, observations, optimizer, loss, weights=None):
    """Fit the model based on inputs with lists of observations
    """
    with tf.GradientTape() as t:
      self.Q_chol = tf.linalg.band_part(
        self.Q_chol_var, -1, 0, name="Q_chol"
      )
      self.R_chol = tf.linalg.band_part(
        self.R_chol_var, -1, 0, name="R_chol"
      )
      self.birth_covs_chol = tf.linalg.band_part(
        self.birth_covs_chol_vars, -1, 0, name="birth_covs_chol"
      )
      outputs = self.call(inputs)
      # tf_weights, tf_means, tf_covs = outputs
      # np_loss = np_observations_loglikelihood(
      #   (tf_weights.numpy(), tf_means.numpy(), tf_covs.numpy()),
      #   observations
      # )
      # print("np_loss: ", np_loss)
      n_k = len(observations)
      if weights is None:
        weights = np.ones((n_k,))
      loss_value = loss(outputs[0], observations[1]) * weights[0]
      for i in range(n_k - 2):
        loss_value = loss(outputs[i+1], observations[i+2]) * weights[i+1]
    grads = t.gradient(loss_value, self.trainable_variables)
    optimizer.apply_gradients(zip(grads, self.trainable_variables))
    return loss_value, outputs

  def call(self, inputs):
    """Implement Linear Multi-target Gaussian Model forward pass.

    During the training phase, the inputs should be composed of the Gaussian
    mixture of the multi-target PHD of the predictor of current time step $k$,
    and the observation at current time step $k$. The forward pass corrects
    the multi-target PHD of the predictor based on the current observation,
    and predict the PHD of the next time step $k+1$.

    During the evaluation phase, the inputs should be composed of the Gaussian
    mixture of the multi-target PHD after corrector at previous time step $k-1$.
    The forward pass calculated the predicted

    Arguments:
      inputs: A list of tensors encodes the weights, means and covariance
        matrices of the initial multi-target PHD in the form of a Gaussian
        mixture, and a list of sensor observations. The weights is of shape
        `[num_gc,]`, means of shape `[num_gc, ndims]`, covariance matrices of
        shape `[num_gc, ndims, ndims]`. The observation is a list of length
        `n_k`, each of shape `[n_obs, n_visible]`.
      training: Boolean or boolean scalar tensor, indicating whether to run
        the model in training mode or inference mode.
      mask: A mask or list of masks. A mask can be either a tensor or None
        (no mask). Ignored at the moment.

    Returns:
      list of three tuples composed of weights, means and Cholesky decomposition
      of the covariance matrix of Gaussian components. The maximum number of
      Gaussian components to keep is determined by the `Jmax` value of the
      model.
    """
    # Acquire the variables for GM-PHD of previous time step
    gm_weights = inputs[0]
    gm_means = inputs[1]
    gm_covs_chol = inputs[2]
    # Get set of observations
    observations = inputs[3]
    n_k = len(observations)
    # Compute the GMs of each time step
    phd_list = []
    for k in range(n_k):
      gm_weights, gm_means, gm_covs_chol = self.gm_append_birth(
        gm_weights, gm_means, gm_covs_chol
      )
      gm_weights, gm_means, gm_covs_chol = self.gm_corrector(
        gm_weights, gm_means, gm_covs_chol, observations[k]
      )
      gm_weights, gm_means, gm_covs_chol = self.gm_predictor(
        gm_weights, gm_means, gm_covs_chol
      )
      gm_weights, gm_means, gm_covs_chol = self.gm_truncate(
        gm_weights, gm_means, gm_covs_chol, self.Jmax
      )
      phd_list.append((gm_weights, gm_means, gm_covs_chol))
    return phd_list

  def gm_truncate(self, gm_weights, gm_means, gm_covs, Jmax):
    """Truncate gaussian mixtures
    """
    sorted_indices = tf.argsort(gm_weights, direction="DESCENDING")
    gather_indices = tf.expand_dims(sorted_indices[:Jmax], axis=1)
    return tf.gather_nd(gm_weights, gather_indices), \
      tf.gather_nd(gm_means, gather_indices), \
      tf.gather_nd(gm_covs, gather_indices)

  def gm_predictor(self, gm_weights, gm_means, gm_covs_chol):
    """GM-PHD Predictor

    The GM predictor processes the list of GMs at time k based on dynamic model.
    """
    # Weights are decayed by target persistence probability $p_s$.
    p_s = tf.nn.sigmoid(self.p_s_logit)
    predictor_weights = gm_weights * p_s
    predictor_means = tf.matmul(gm_means, self.F, transpose_b=True)
    # In order to broadcast F with multiple GM covariance matrices, F needs to
    # be broadcasted into same dimensions as gm_covs
    F_padded = tf.broadcast_to(
      self.F, tf.concat([tf.shape(gm_covs_chol)[:-2], tf.shape(self.F)], axis=0)
    )
    F_cov_chols = tf.matmul(F_padded, gm_covs_chol)
    Q = tf.matmul(self.Q_chol, tf.transpose(self.Q_chol))
    predictor_covs = tf.matmul(
      F_cov_chols, tf.transpose(F_cov_chols, perm=[0, 2, 1])
    ) + Q
    try:
      predictor_covs_chol = tf.linalg.cholesky(
        predictor_covs, name="predictor_covs_chol"
      )
    except:
      tf.print(predictor_covs)
    return predictor_weights, predictor_means, predictor_covs_chol

  def gm_append_birth(self, gm_weights, gm_means, gm_covs_chol):
    """GM-PHD Append birth PHD to existing PHD
    """
    new_weights = tf.concat([gm_weights, self.birth_weights], axis=0)
    new_means = tf.concat([gm_means, self.birth_means], axis=0)
    new_covs = tf.concat([gm_covs_chol, self.birth_covs_chol], axis=0)
    return new_weights, new_means, new_covs

  def gm_corrector(self, gm_weights, gm_means, gm_covs_chol, observations):
    """GM-PHD Corrector
    """
    # The first part of the posterior PHD is composed of un-detected targets.
    p_d = tf.nn.sigmoid(self.p_d_logit)
    undetected_weights = gm_weights - p_d * gm_weights
    undetected_means = gm_means
    undetected_covs_chol = gm_covs_chol
    # Corrected GMs
    hm = self._corrector_compute_hm(gm_means=gm_means)
    r_hph = self._corrector_compute_r_hph(gm_covs_chol=gm_covs_chol)
    # K = self._corrector_compute_k(gm_covs_chol=gm_covs_chol, r_hph=r_hph)
    qz = self._corrector_compute_qz(
      observations=observations, hm=hm, r_hph=r_hph
    )
    wq = self._corrector_compute_wq(gm_weights=gm_weights, qz=qz)
    w_den = self._corrector_compute_weights_denominator(wq=wq)
    corrected_weights = tf.reshape(self._corrector_compute_weights(
      w_den=w_den, wq=wq
    ), shape=[-1])
    # The inverse of r_hph is used in calculation of means and covs.
    # So, we compute it here.
    r_hph_chol = tf.linalg.cholesky(r_hph)
    r_hph_chol_inv = tf.linalg.inv(r_hph_chol)
    r_hph_inv_chol = tf.transpose(r_hph_chol_inv, perm=[0, 2, 1])
    corrected_means = tf.reshape(self._corrector_compute_means(
      gm_means=gm_means, gm_covs_chol=gm_covs_chol,
      observations=observations,
      r_hph_inv_chol=r_hph_inv_chol, hm=hm
    ), shape=[-1, self.ndims])
    corrected_covs_chol = tf.reshape(self._corrector_compute_covs_chol(
      gm_covs_chol=gm_covs_chol,
      r_hph_inv_chol=r_hph_inv_chol,
      n_obs=tf.shape(observations)[0]
    ), shape=[-1, self.ndims, self.ndims])
    return tf.concat([undetected_weights, corrected_weights], axis=0), \
      tf.concat([undetected_means, corrected_means], axis=0), \
      tf.concat([undetected_covs_chol, corrected_covs_chol], axis=0)

  def _corrector_compute_weights_denominator(self, wq):
    """Compute the denominator of weights in the posterior PHD after the GM-PHD
    corrector.

    Args:
      wq: The $w^{(j)}q^{(j)}(z) term in the GM-PHD corrector weight
        calculation, of shape `[num_obs, num_gc]`.

    Returns:
      The denominator of weights in the posterior PHD after the GM-PHD
      corrector, of shape `[num_obs,]`.
    """
    p_d = tf.nn.sigmoid(self.p_d_logit)
    w_den = self.clutter_lam * self.clutter_z + p_d * tf.reduce_sum(
      wq, axis=1
    )
    return w_den

  def _corrector_compute_weights(self, w_den, wq):
    """Compute the weights of the Gaussian components in the posterior PHD after
    the corrector

    Args:
      w_den: denominator of the weights, of shape `[num_obs,]`.
      wq: The $w^{(j)}q^{(j)}(z) term in the GM-PHD corrector weight
        calculation, of shape `[num_obs, num_gc]`.

    Returns:
      the weights of the Gaussian components in the posterior PHD after
      the corrector, of shape `[num_obs, num_gc]`.
    """
    # w_den_padded: `[num_obs, num_gc]`
    w_den_padded = tf.transpose(tf.broadcast_to(
      w_den, shape=tf.concat([tf.shape(wq)[1:], tf.shape(w_den)], axis=0)
    ))
    return tf.divide(wq, w_den_padded)

  def _corrector_compute_wq(self, gm_weights, qz):
    """Compute the $w^{(j)}q^{(j)}(z) term in the GM-PHD corrector
    weight calculation.

    Args:
      gm_weights: The weights of Gaussian components in the prior PHD, of shape
        `[num_gc,]`.
      qz: The $q^{(j)}(z)$ term in the GM-PHD corrector, of shape
        `[num_obs, num_gc]`

    Returns:
      the $w^{(j)}q^{(j)}(z) term in the GM-PHD corrector weight calculation,
      of shape `[num_obs, num_gc]`.
    """
    # weights_padded: `[num_obs, num_gc]`
    weights_padded = tf.broadcast_to(
      gm_weights, shape=tf.concat(
        [tf.shape(qz)[:-1], tf.shape(gm_weights)],
        axis=0
      )
    )
    wq = tf.multiply(weights_padded, qz)
    return wq

  def _corrector_compute_qz(self, observations, hm, r_hph):
    """Compute the $q^{(j)}(z)$ term in the GM-PHD corrector.

    :math:`q_k^{(j)}(z) = \mathcal{N}(z; Hm^{(j)}, R+HP^{(j)}H^T)`

    Args:
      observations: Sensor observations of the targets, of shape
        `[num_obs, n_visible}`.
      hm: Term Hm in the GM-PHD corrector, of shape `[num_gc, n_visible]`.
      r_hph: (R + HPH^T) term of shape `[num_gc, n_visible, n_visible]`.

    Returns:
      The $q^{(j)}(z)$ term in the GM-PHD corrector, of shape
      `[num_obs, num_gc]`
    """
    q_dists = tfp.distributions.MultivariateNormalFullCovariance(
      loc=hm, covariance_matrix=r_hph, name="q_dists"
    )
    obs_padded = tf.transpose(
      tf.broadcast_to(
        observations,
        shape=tf.concat([tf.shape(hm)[:-1], tf.shape(observations)], axis=0)
      ), perm=[1, 0, 2]
    )
    qz = q_dists.prob(obs_padded, name="q_z")
    return qz

  def _corrector_compute_means(self, gm_means, gm_covs_chol,
                               observations,
                               r_hph_inv_chol, hm):
    """Compute the means of the Gaussian components in the posterior
    GM-PHD after the corrector.

    :math:`m_k^{(j)}(z) = m^{(j)} + K(z - Hm^{(j)})`

    Args:
      gm_means: Mean vectors for Gaussian components, of shape
        `[num_gc, ndims]`.
      observations: Sensor observations of the targets, of shape
        `[num_obs, n_visible}`.
      r_hph_inv_chol: Cholesky decomposition of (R+HPH^T)^{-1}, of shape
        `[num_gc, ndims, n_dims]`
      hm: Term Hm in the GM-PHD corrector, of shape `[num_gc, n_visible]`.

    Returns:
      The means of the Gaussian components in the posterior GM-PHD after
      the corrector, of shape `[num_obs, num_gc, ndims]`.
    """
    # hm_padded: `[num_obs, num_gc, n_visible]`
    hm_padded = tf.broadcast_to(
      hm, shape=tf.concat([tf.shape(observations)[:-1], tf.shape(hm)], axis=0)
    )
    # obs_padded: `[num_obs, num_gc, n_visible]`
    obs_padded = tf.transpose(
      tf.broadcast_to(
        observations,
        shape=tf.concat([tf.shape(gm_means)[:-1], tf.shape(observations)],
                        axis=0)
      ), perm=[1, 0, 2]
    )
    # z_hm = obs_padded - hm_padded, of shape `[num_obs, num_gc, n_visible, 1]`
    z_hm = tf.expand_dims(obs_padded - hm_padded, axis=-1)
    # means_padded: `[num_obs, num_gc, ndims, 1]`
    means_padded = tf.expand_dims(tf.broadcast_to(
      gm_means, shape=tf.concat([tf.shape(observations)[:-1],
                                 tf.shape(gm_means)], axis=0)
    ), axis=-1)
    # Calculate K
    H_T = tf.transpose(self.H)
    # H_padded shape: `[num_gc, ndims, n_visible]`
    H_T_padded = tf.broadcast_to(
      H_T, tf.concat([tf.shape(r_hph_inv_chol)[:-2], H_T.shape], axis=0)
    )
    gm_covs = tf.matmul(gm_covs_chol,
                        tf.transpose(gm_covs_chol, perm=[0, 2, 1]))
    r_hph_inv = tf.matmul(r_hph_inv_chol,
                          tf.transpose(r_hph_inv_chol, perm=[0, 2, 1]))
    K = tf.matmul(gm_covs, tf.matmul(H_T_padded, r_hph_inv))
    # K_padded: `[num_obs, num_gc, n_dims, n_visible]`
    K_padded = tf.broadcast_to(
      K, shape=tf.concat([tf.shape(observations)[:-1], tf.shape(K)], axis=0)
    )
    # means: `[num_obs, num_gc, n_dims, 1]`
    means = means_padded + tf.matmul(K_padded, z_hm)
    means = tf.reshape(means, shape=tf.shape(means)[:-1])
    return means

  def _corrector_compute_covs_chol(self, gm_covs_chol, r_hph_inv_chol, n_obs):
    """Compute the covariance matrices for the Gaussian components in the
    posterior GM-PHD after the corrector

    :math:`P_k^{(j)} = [I - K^{(j)} H] P^{j}

    Args:
      gm_covs: Covariance matrices of Gaussian components, of shape
        `[num_gc, ndims, ndims]`.
      K: Term K in GM-PHD corrector, of shape `[num_gc, ndims, n_visible]`
      n_obs: Number of observations at current time step.

    Returns:
      Corrected covariance matrics of shape `[num_obs, num_gc, ndims, ndims]`
    """
    H_T = tf.transpose(self.H)
    # H_padded shape: `[num_gc, ndims, n_visible]`
    H_T_padded = tf.broadcast_to(
      H_T, tf.concat([tf.shape(r_hph_inv_chol)[:-2], H_T.shape], axis=0)
    )
    gm_covs = tf.matmul(gm_covs_chol,
                        tf.transpose(gm_covs_chol, perm=[0, 2, 1]))
    #KHP_chol
    KHP_chol = tf.matmul(gm_covs,
                         tf.matmul(H_T_padded, r_hph_inv_chol))
    # Compute covariance
    covs = gm_covs - tf.matmul(KHP_chol,
                               tf.transpose(KHP_chol, perm=[0, 2, 1]))
    try:
      covs_chol = tf.linalg.cholesky(covs)
    except:
      tf.print(covs)
    # nforce positive definite
    # covs = tf_check_covs(covs)
    covs_chol_padded = tf.broadcast_to(
      covs_chol, shape=tf.concat([[n_obs], tf.shape(covs_chol)], axis=0)
    )
    return covs_chol_padded

  def _corrector_compute_hm(self, gm_means):
    """Compute Hm term in the GM-PHD corrector

    :math:`Hm^{j}`

    Args:
      gm_means: Mean vectors for Gaussian components, of shape
        `[num_gc, ndims]`

    Returns:
      Hm term in the GM-PHD corrector, of shape `[num_gc, n_visible]`.
    """
    hm = tf.matmul(gm_means, self.H, transpose_b=True, name="hm")
    return hm

  # def _corrector_compute_k(self, gm_covs_chol, r_hph):
  #   """Compute term K in the GM-PHD corrector
  #
  #   :math:`K^{(j)}=P^{(j)}H^T(HP^{(j)}H^T + R)^{-1}`
  #
  #   Args:
  #     gm_covs: Covariance matrices of Gaussian components, of shape
  #       `[num_gc, ndims, ndims]`.
  #     r_hph: (R + HPH^T) term of shape `[num_gc, n_visible, n_visible]`.
  #
  #   Returns:
  #     Term $K$ in GM-PHD corrector, of shape `[num_gc, ndims, n_visible]`.
  #   """
  #   H_T = tf.transpose(self.H)
  #   # H_padded shape: `[num_gc, ndims, n_visible]`
  #   H_T_padded = tf.broadcast_to(
  #     H_T, tf.concat([tf.shape(gm_covs)[:-2], H_T.shape], axis=0)
  #   )
  #   # In order to guarantee the inverse is positive definite, do
  #   # Cholesky decomposition, inverse, and then compute K.
  #   r_hph_chol = tf.linalg.cholesky(r_hph)
  #   r_hph_chol_inv = tf.linalg.inv(r_hph_chol)
  #   r_hph_inv = tf.matmul(tf.transpose(r_hph_chol_inv, perm=[0, 2, 1]),
  #                         r_hph_chol_inv)
  #   K = tf.matmul(gm_covs, tf.matmul(H_T_padded, r_hph_inv))
  #   return K

  def _corrector_compute_r_hph(self, gm_covs_chol):
    """Compute (R + HPH^T) term in the GM-PHD corrector

    :math:`H_k P_{k|k-1}^{(j)}H_k^T + R_{k}`

    Args:
      gm_covs_chol: Cholesky decomposition of covariance matrices of Gaussian
        components, of shape `[num_gc, ndims, ndims]`.

    Returns:
      (R + HPH^T) calculated based on each Gaussian components, of shape
      `[num_gc, n_visible, n_visible]`.
    """
    R = tf.matmul(self.R_chol, tf.transpose(self.R_chol))
    R_padded = tf.broadcast_to(
      R, tf.concat([tf.shape(gm_covs_chol)[:-2], self.R_chol.shape], axis=0),
      name="R_padded"
    )
    # H_padded shape: `[num_gc, n_visible, ndims]`
    H_padded = tf.broadcast_to(
      self.H, tf.concat([tf.shape(gm_covs_chol)[:-2], self.H.shape], axis=0)
    )
    hph_chol = tf.matmul(H_padded, gm_covs_chol)
    r_hph = R_padded + tf.matmul(
      hph_chol, tf.transpose(hph_chol, perm=[0, 2, 1])
    )
    return r_hph

class TfLinearMTGMBatched(TfLinearMTGM):
  """Inherit all TfLinearMTGM, except for fitting
  """
  def fit(self, inputs, observations, optimizer, loss):
    with tf.GradientTape() as t:
      outputs = self.call(inputs)
      # tf_weights, tf_means, tf_covs = outputs
      # np_loss = np_observations_loglikelihood(
      #   (tf_weights.numpy(), tf_means.numpy(), tf_covs.numpy()),
      #   observations
      # )
      # print("np_loss: ", np_loss)
      loss_value = loss(outputs, observations)
    grads = t.gradient(loss_value, self.trainable_variables)
    optimizer.apply_gradients(zip(grads, self.trainable_variables))
    return loss_value, outputs


class TfLinearMTGM_ID_Model(TfLinearMTGM):
  """Linear Multi-target Gaussian Mixture Model with Track ID

  Question: Do I need it?

  This linear Multi-target Gaussian Mixture model is implemented with tensorflow
  packages.

  Args:
    n_visible: number of visible dims
    n_hidden: number of hidden dims
    n_birth: number of birth component in the model

  Attributes:
    ndims: dimensionality of state vector.
    n_visible: number of visible dims
    n_hidden: number of hidden dims
    n_birth: number of birth component in the model
    p_d_logit: Logit, used to compute detection probability p_d using sigmoid
      function. It guarantees the boundary of p_d during training.
    p_s_logit: Logit, used to compute survival probability p_s using sigmoid
      function. It guarantees the boundary of p_s during training.
    F: Variable (trainable), the linear multiplier of the dynamic model of shape
      `[n_visible + n_hidden, n_visible + n_hidden]`.
  """
  def __init__(self,
               n_visible,
               n_hidden,
               n_birth):
    super(TfLinearMTGM_ID_Model, self).__init__(
      n_visible=n_visible, n_hidden=n_hidden, n_birth=n_birth
    )
    # Add track ID field to state dimensions
    self.ndims = n_visible + n_hidden + 1
    # Linear Multiplier
    self.F_train = self.F
    self.F = tf.concat([
      tf.concat([
        self.F_train,
        tf.zeros((1, self.ndims-1), dtype=tf.float64)
      ], axis=0),
      tf.concat([
        tf.zeros((self.ndims-1, 1), dtype=tf.float64),
        tf.ones((1, 1), dtype=tf.float64)
      ], axis=0)
    ], axis=1)
    # Q_dev
    self.Q_dev_train = self.Q_dev
    self.Q_dev = tf.concat([
      tf.concat([
        self.Q_dev_train,
        tf.zeros((1, self.ndims-1), dtype=tf.float64)
      ], axis=0),
      tf.concat([
        tf.zeros((self.ndims-1, 1), dtype=tf.float64),
        tf.ones((1, 1), dtype=tf.float64)
      ], axis=0)
    ], axis=1)
    # H needs expanding
    self.H = tf.concat([
      self.H, tf.zeros((self.n_visible, 1), dtype=tf.float64)
    ], axis=1)
    # No change to R_dev
    self.R_dev_train = self.R_dev
    # In the birth PHD, means and covariance matrix needs to be padded
    self.birth_means_train = self.birth_means
    self.birth_means = tf.concat([
      self.birth_means_train,
      tf.zeros((self.n_birth, 1), dtype=tf.float64)
    ], axis=1)
    self.birth_covs_dev_train = self.birth_covs_dev
    self.birth_covs_dev = tf.concat([
      tf.concat([
        self.Q_dev_train,
        tf.zeros((self.n_birth, 1, self.ndims-1), dtype=tf.float64)
      ], axis=1),
      tf.concat([
        tf.zeros((self.n_birth, self.ndims-1, 1), dtype=tf.float64),
        tf.ones((self.n_birth, 1, 1), dtype=tf.float64)
      ], axis=1)
    ], axis=2)
    # Next track ID
    self.next_track_id = 1
