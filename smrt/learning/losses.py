###############################################################################
# Copyright (c) 2018-2019, Tinghui Wang
# License: BSD 3-Clause
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###############################################################################
import scipy.stats
import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp

__all__ = [
  'np_observations_loglikelihood',
  'tf_observations_loglikelihood',
  'tf_observations_loglikelihood_v2',
  'tf_mtt_uniform_nce',
  'tf_mtt_uniform_nce_v2',
  'tf_mtt_categorical_nce',
  'tf_mrt_nce'
]

def np_observations_loglikelihood(inputs, observations):
  # Acquire the variables for GM-PHD of previous time step
  gm_weights = inputs[0]
  gm_means = inputs[1]
  gm_covs = inputs[2]
  ndims = observations.shape[1]
  # Calculate results
  z_length = observations.shape[0]
  gc_length = gm_weights.shape[0]
  # Calculate results
  logloss = np.zeros((z_length, gc_length), np.float)
  for j in range(gc_length):
    for i in range(z_length):
      try:
        logloss[i, j] = scipy.stats.multivariate_normal.logpdf(
          x=observations[i, :],
          mean=gm_means[j, :ndims],
          cov=gm_covs[j, :ndims, :ndims]
        ) * gm_weights[j]
      except ValueError as _error:
        print("ngc: ", j, " out of ", gc_length)
        print("weight: ", gm_weights[j])
        print("covariance: \r\n", gm_covs[j, :, :])
        raise _error
  return - np.sum(logloss)


def tf_observations_loglikelihood(inputs, observations):
  # Acquire the variables for GM-PHD of previous time step
  # print(observations)
  gm_weights = inputs[0]
  gm_means = inputs[1]
  gm_covs = inputs[2]
  ndims = observations.shape[1]
  observations = tf.convert_to_tensor(observations, tf.float64)
  # get distribution
  q_dists = tfp.distributions.MultivariateNormalFullCovariance(
    loc=gm_means[:, :ndims], covariance_matrix=gm_covs[:, :ndims, :ndims],
    name="q_dists"
  )
  obs_padded = tf.transpose(
    tf.broadcast_to(
      observations,
      shape=tf.concat([tf.shape(gm_means)[:-1], tf.shape(observations)],
                      axis=0)
    ), perm=[1, 0, 2]
  )
  logloss_full = q_dists.log_prob(obs_padded, name="log_prob")
  weights_padded = tf.broadcast_to(
    gm_weights,
    shape=tf.concat([tf.shape(observations)[:-1], tf.shape(gm_weights)],
                    axis=0)
  )
  logloss = -1 * tf.reduce_sum(
    tf.multiply(weights_padded, logloss_full)
  )
  return logloss

def tf_observations_loglikelihood_v2(inputs, observations):
  # Acquire the variables for GM-PHD of previous time step
  # print(observations)
  gm_weights = inputs[0]
  gm_means = inputs[1]
  gm_covs_chol = inputs[2]
  ndims = observations.shape[1]
  observations = tf.convert_to_tensor(observations, tf.float64)
  # get distribution
  q_dists = tfp.distributions.MultivariateNormalTriL(
    loc=gm_means[:, :ndims], scale_tril=gm_covs_chol[:, :ndims, :ndims],
    name="q_dists"
  )
  obs_padded = tf.transpose(
    tf.broadcast_to(
      observations,
      shape=tf.concat([tf.shape(gm_means)[:-1], tf.shape(observations)],
                      axis=0)
    ), perm=[1, 0, 2]
  )
  logloss_full = q_dists.log_prob(obs_padded, name="log_prob")
  weights_padded = tf.broadcast_to(
    gm_weights,
    shape=tf.concat([tf.shape(observations)[:-1], tf.shape(gm_weights)],
                    axis=0)
  )
  logloss = -1 * tf.reduce_sum(
    tf.multiply(weights_padded, logloss_full)
  )
  return logloss


def tf_mtt_uniform_nce(inputs,
                       observations,
                       num_samples,
                       minval=-1.,
                       maxval=1.,
                       epsilon=1e-6):
  """To maximize the likelihood of multi-target tracking, we need to implement
  negative sampling in the same way as noise contrastive estimation.
  """
  gm_weights = inputs[0]
  gm_means = inputs[1]
  gm_covs = inputs[2]
  ndims = observations.shape[1]
  neg_samples = tf.random.uniform(
    shape=(num_samples, ndims),
    minval=minval,
    maxval=maxval,
    dtype=tf.float64
  )
  # Acquire the variables for GM-PHD of previous time step
  # print(observations)
  observations = tf.convert_to_tensor(observations, tf.float64)
  # get distribution
  q_dists = tfp.distributions.MultivariateNormalFullCovariance(
    loc=gm_means[:, :ndims], covariance_matrix=gm_covs[:, :ndims, :ndims],
    name="q_dists"
  )
  # Padded observation, shape `[num_obs, num_gc, n_visible]`
  obs_padded = tf.transpose(
    tf.broadcast_to(
      observations,
      shape=tf.concat([gm_means.shape[:-1], observations.shape], axis=0)
    ), perm=[1, 0, 2]
  )
  # Padded GM probability, shape `[num_obs, num_gc]`
  pos_gm_prob_full = q_dists.prob(obs_padded, name="pos_gm_prob")
  # Padded weights: shape `[num_obs, num_gc]`
  weights_padded = tf.broadcast_to(
    gm_weights,
    shape=tf.concat([observations.shape[:-1], gm_weights.shape], axis=0)
  )
  # Positive Probability clamped
  pos_prob = tf.clip_by_value(
    tf.reduce_sum(tf.multiply(weights_padded, pos_gm_prob_full), axis=1),
    clip_value_min=epsilon, clip_value_max=1. - epsilon, name="pos_prob_clipped"
  )
  # Positive log likelihood
  pos_log_loss = tf.reduce_sum(tf.math.log(pos_prob), name="pos_log_loss")
  # Negative padded samples: `[num_samples, num_gc, n_visible]`.
  neg_padded = tf.transpose(
    tf.broadcast_to(
      neg_samples,
      shape=tf.concat([gm_means.shape[:-1], neg_samples.shape], axis=0)
    ), perm=[1, 0, 2]
  )
  # Negative GM probability, shape `[num_samples, num_gc]`.
  neg_gm_prob_full = q_dists.prob(neg_padded, name="neg_gm_prob")
  # Negative weights padded: `[num_samples, num_gc]`
  neg_weights_padded = tf.broadcast_to(
    gm_weights,
    shape=tf.concat([neg_samples.shape[:-1], gm_weights.shape], axis=0)
  )
  # Positive Probability clamped
  neg_prob = tf.clip_by_value(
    tf.reduce_sum(tf.multiply(neg_weights_padded, neg_gm_prob_full), axis=1),
    clip_value_min=epsilon, clip_value_max=1.-epsilon, name="neg_prob_clipped"
  )
  # Positive log likelihood
  neg_log_loss = tf.reduce_sum(tf.math.log(1. - neg_prob), name="pos_log_loss")
  logloss = -1 * (neg_log_loss + pos_log_loss)
  return logloss


def tf_mtt_uniform_nce_v2(inputs,
                          observations,
                          num_samples,
                          minval=-1.,
                          maxval=1.,
                          epsilon=1e-6):
  """To maximize the likelihood of multi-target tracking, we need to implement
  negative sampling in the same way as noise contrastive estimation.
  """
  gm_weights = inputs[0]
  gm_means = inputs[1]
  gm_covs_chol = inputs[2]
  ndims = observations.shape[1]
  neg_samples = tf.random.uniform(
    shape=(num_samples, ndims),
    minval=minval,
    maxval=maxval,
    dtype=tf.float64
  )
  # Acquire the variables for GM-PHD of previous time step
  # print(observations)
  observations = tf.convert_to_tensor(observations, tf.float64)
  # get distribution
  q_dists = tfp.distributions.MultivariateNormalTriL(
    loc=gm_means[:, :ndims], scale_tril=gm_covs_chol[:, :ndims, :ndims],
    name="q_dists"
  )
  # Padded observation, shape `[num_obs, num_gc, n_visible]`
  obs_padded = tf.transpose(
    tf.broadcast_to(
      observations,
      shape=tf.concat([tf.shape(gm_means)[:-1], tf.shape(observations)], axis=0)
    ), perm=[1, 0, 2]
  )
  # Padded GM probability, shape `[num_obs, num_gc]`
  pos_gm_prob_full = q_dists.prob(obs_padded, name="pos_gm_prob")
  # Padded weights: shape `[num_obs, num_gc]`
  weights_padded = tf.broadcast_to(
    gm_weights,
    shape=tf.concat([tf.shape(observations)[:-1], tf.shape(gm_weights)], axis=0)
  )
  # Positive Probability clamped
  pos_prob = tf.clip_by_value(
    tf.reduce_sum(tf.multiply(weights_padded, pos_gm_prob_full), axis=1),
    clip_value_min=epsilon, clip_value_max=1. - epsilon, name="pos_prob_clipped"
  )
  # Positive log likelihood
  pos_log_loss = tf.reduce_sum(tf.math.log(pos_prob), name="pos_log_loss")
  # Negative padded samples: `[num_samples, num_gc, n_visible]`.
  neg_padded = tf.transpose(
    tf.broadcast_to(
      neg_samples,
      shape=tf.concat([tf.shape(gm_means)[:-1], tf.shape(neg_samples)], axis=0)
    ), perm=[1, 0, 2]
  )
  # Negative GM probability, shape `[num_samples, num_gc]`.
  neg_gm_prob_full = q_dists.prob(neg_padded, name="neg_gm_prob")
  # Negative weights padded: `[num_samples, num_gc]`
  neg_weights_padded = tf.broadcast_to(
    gm_weights,
    shape=tf.concat([tf.shape(neg_samples)[:-1], tf.shape(gm_weights)], axis=0)
  )
  # Positive Probability clamped
  neg_prob = tf.clip_by_value(
    tf.reduce_sum(tf.multiply(neg_weights_padded, neg_gm_prob_full), axis=1),
    clip_value_min=epsilon, clip_value_max=1.-epsilon, name="neg_prob_clipped"
  )
  # Positive log likelihood
  neg_log_loss = tf.reduce_sum(tf.math.log(1. - neg_prob), name="pos_log_loss")
  logloss = -1 * (neg_log_loss + pos_log_loss)
  return logloss

#####
# Save the following as comments for future reference if anything failed.
#####
# def tf_mtt_uniform_nce(inputs,
#                        observations,
#                        num_samples,
#                        minval=-1.,
#                        maxval=1.):
#   """To maximize the likelihood of multi-target tracking, we need to implement
#   negative sampling in the same way as noise contrastive estimation.
#   """
#   gm_weights = inputs[0]
#   gm_means = inputs[1]
#   gm_covs = inputs[2]
#   ndims = observations.shape[1]
#   neg_samples = tf.random.uniform(
#     shape=(num_samples, ndims),
#     minval=minval,
#     maxval=maxval,
#     dtype=tf.float64
#   )
#   # Acquire the variables for GM-PHD of previous time step
#   # print(observations)
#   observations = tf.convert_to_tensor(observations, tf.float64)
#   # get distribution
#   q_dists = tfp.distributions.MultivariateNormalFullCovariance(
#     loc=gm_means[:, :ndims], covariance_matrix=gm_covs[:, :ndims, :ndims],
#     name="q_dists"
#   )
#   # Padded observation, shape `[num_obs, num_gc, n_visible]`
#   obs_padded = tf.transpose(
#     tf.broadcast_to(
#       observations,
#       shape=tf.concat([gm_means.shape[:-1], observations.shape], axis=0)
#     ), perm=[1, 0, 2]
#   )
#   # Padded GM probability, shape `[num_obs, num_gc]`
#   pos_gm_prob_full = q_dists.prob(obs_padded, name="pos_gm_prob")
#   # Padded weights: shape `[num_obs, num_gc]`
#   weights_padded = tf.broadcast_to(
#     gm_weights,
#     shape=tf.concat([observations.shape[:-1], gm_weights.shape], axis=0)
#   )
#   # Positive Probability clamped
#   pos_prob = tf.clip_by_value(
#     tf.reduce_sum(tf.multiply(weights_padded, pos_gm_prob_full), axis=1),
#     clip_value_min=0., clip_value_max=1., name="pos_prob_clipped"
#   )
#   # Positive log likelihood
#   pos_log_loss = tf.reduce_sum(tf.math.log(pos_prob), name="pos_log_loss")
#   # Negative padded samples: `[num_samples, num_gc, n_visible]`.
#   neg_padded = tf.transpose(
#     tf.broadcast_to(
#       neg_samples,
#       shape=tf.concat([gm_means.shape[:-1], neg_samples.shape], axis=0)
#     ), perm=[1, 0, 2]
#   )
#   # Negative GM probability, shape `[num_samples, num_gc]`.
#   neg_gm_prob_full = q_dists.prob(neg_padded, name="neg_gm_prob")
#   # Negative weights padded: `[num_samples, num_gc]`
#   neg_weights_padded = tf.broadcast_to(
#     gm_weights,
#     shape=tf.concat([neg_samples.shape[:-1], gm_weights.shape], axis=0)
#   )
#   # Positive Probability clamped
#   neg_prob = tf.clip_by_value(
#     tf.reduce_sum(tf.multiply(neg_weights_padded, neg_gm_prob_full), axis=1),
#     clip_value_min=0., clip_value_max=1., name="neg_prob_clipped"
#   )
#   # Positive log likelihood
#   neg_log_loss = tf.reduce_sum(tf.math.log(1. - neg_prob), name="pos_log_loss")
#   logloss = -1 * (neg_log_loss + pos_log_loss)
#   return logloss


def tf_mtt_categorical_nce(inputs,
                           observations,
                           num_samples):
  """To maximize the likelihood of multi-target tracking, we need to implement
  negative sampling in the same way as noise contrastive estimation.
  """
  gm_weights = inputs[0]
  gm_means = inputs[1]
  gm_covs = inputs[2]
  ndims = observations.shape[1]
  neg_samples_indices = tf.reshape(tf.random.categorical(
    tf.math.log(gm_weights), num_samples=num_samples
  ), shape=[-1])
  neg_samples = tf.nn.embedding_lookup(gm_means, neg_samples_indices)
  # Acquire the variables for GM-PHD of previous time step
  # print(observations)
  observations = tf.convert_to_tensor(observations, tf.float64)
  # get distribution
  q_dists = tfp.distributions.MultivariateNormalFullCovariance(
    loc=gm_means[:, :ndims], covariance_matrix=gm_covs[:, :ndims, :ndims],
    name="q_dists"
  )
  obs_padded = tf.transpose(
    tf.broadcast_to(
      observations,
      shape=tf.concat([gm_means.shape[:-1], observations.shape], axis=0)
    ), perm=[1, 0, 2]
  )
  pos_logloss_full = q_dists.log_prob(obs_padded, name="pos_log_prob")
  neg_padded = tf.transpose(
    tf.broadcast_to(
      neg_samples,
      shape=tf.concat([gm_means.shape[:-1], neg_samples.shape], axis=0)
    ), perm=[1, 0, 2]
  )
  neg_logloss_full = q_dists.log_prob(neg_padded, name="neg_log_prob")
  weights_padded = tf.broadcast_to(
    gm_weights,
    shape=tf.concat([observations.shape[:-1], gm_weights.shape], axis=0)
  )
  neg_weights_padded = tf.broadcast_to(
    gm_weights,
    shape=tf.concat([neg_samples.shape[:-1], gm_weights.shape], axis=0)
  )
  logloss = -1 * tf.reduce_sum(
    tf.multiply(weights_padded, pos_logloss_full)
  ) + tf.reduce_sum(
    tf.multiply(neg_weights_padded, neg_logloss_full)
  )
  return logloss


def tf_mrt_nce(inputs, observations, n_observations):
  """Loss function for multi-resident tracking with negative sampling.

  The multi-resident tracking negative contrast sampling loss function use
  all the inactive sensor (represented in the form of vector embeddings) as
  negative samples. The loss function is similar to cross-entropy given
  sensor observations at time `k` as positive labels and vectors of inactive
  sensors as negative labels.

  ```
    \mathcal{L} = \frac{1}{|S|} \sum_{\mathbf{z}\in S}
    (-1)^{\mathbf{1}\left(\mathbf{z}\in Z^{(k)}\right)} \log D_k(\mathbf{z})
  ```

  Args:
    inputs: tuples of tf.Tensors, composed of (gm_weights, gm_means, gm_covs)
    observations: vector embeddings of sensor observations
    n_observations: negatve sensor observations

  Returns:
    tf.Tensor, the multi-resident tracking negative sampling loss
  """
  gm_weights = inputs[0]
  gm_means = inputs[1]
  gm_covs = inputs[2]
  ndims = observations.shape[1]
  observations = tf.convert_to_tensor(observations, tf.float64)
  n_observations = tf.convert_to_tensor(n_observations, tf.float64)
  # get distribution
  q_dists = tfp.distributions.MultivariateNormalFullCovariance(
    loc=gm_means[:, :ndims], covariance_matrix=gm_covs[:, :ndims, :ndims],
    name="q_dists"
  )
  # Padding observations
  obs_padded = tf.transpose(
    tf.broadcast_to(
      observations,
      shape=tf.concat([gm_means.shape[:-1], observations.shape], axis=0)
    ), perm=[1, 0, 2]
  )
  # Padding negative observations
  n_obs_padded = tf.transpose(
    tf.broadcast_to(
      n_observations,
      shape=tf.concat([gm_means.shape[:-1], n_observations.shape], axis=0)
    ), perm=[1, 0, 2]
  )
  # Compute loss
  pos_logloss_full = q_dists.log_prob(obs_padded, name="pos_log_prob")
  neg_logloss_full = q_dists.log_prob(n_obs_padded, name="neg_log_prob")
  # Padded weights for each Gaussian Components
  weights_padded = tf.broadcast_to(
    gm_weights,
    shape=tf.concat([observations.shape[:-1], gm_weights.shape], axis=0)
  )
  neg_weights_padded = tf.broadcast_to(
    gm_weights,
    shape=tf.concat([n_observations.shape[:-1], gm_weights.shape], axis=0)
  )
  logloss = -1 * tf.reduce_sum(
    tf.multiply(weights_padded, pos_logloss_full)
  ) + tf.reduce_sum(
    tf.multiply(neg_weights_padded, neg_logloss_full)
  )
  return logloss
