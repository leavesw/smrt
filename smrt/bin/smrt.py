"""SMRT resident tracking
"""
import argparse
import logging
import os
import ruamel.yaml as yaml

import smrt
from smrt.bin.smrt_preprocessing import SMRT_Preprocessing
from smrt.bin.smrt_sensor_vectorization import SMRT_SensorVectorization
from smrt.bin.smrt_tracking import smrt_tracking
from smrt.bin.smrt_post_processing import smrt_post_processing


def parse_args() -> argparse.Namespace:
    """Parse command line arguments to smrt.
    """
    parser = argparse.ArgumentParser(
        prog="smrt", description="sMRT: Multi-resident tracking based on sensor vectorization"
    )
    parser.add_argument(
        "-d",
        "--dataset",
        required=True,
        help="Path to pickled dataset file generated during pre-processing step.",
    )
    parser.add_argument(
        "-c", "--config", default=None, required=False, help="SMRT configuration file"
    )
    parser.add_argument(
        "-l",
        "--length",
        required=False,
        default=None,
        type=int,
        help="Length of sensor observations to process.",
    )
    parser.add_argument(
        "-m", "--metrics", action="store_true", default=False, help="Computing SMRT metrics."
    )
    parser.add_argument(
        "-o",
        "--output",
        required=True,
        help="Directory to save temporary sensor vectorization file.",
    )
    parser.add_argument("-v", "--verbose", action="count", default=0, help="Logging verbosity.")
    parser.add_argument("--version", action="version", version="%(prog)s " + smrt.version)
    return parser.parse_args()


def logging_setup(
    output_dir: str,
    filename: str = "smrt.log",
    level: int = logging.INFO,
) -> None:
    """Setup logging.

    Args:
        output_dir: Directory to save logs.
        filename: Filename of the log.
        level: Verbosity level.
    """
    formatter = logging.Formatter(
        "[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} "
        "%(levelname)s - %(message)s",
        "%m-%d %H:%M:%S"
    )
    console = logging.StreamHandler()
    console.setLevel(level)
    console.setFormatter(formatter)
    file_handler = logging.FileHandler(os.path.join(output_dir, filename))
    file_handler.setLevel(level)
    file_handler.setFormatter(formatter)
    root_logger = logging.getLogger()
    root_logger.addHandler(console)
    root_logger.addHandler(file_handler)
    app_name = os.path.splitext(os.path.basename(__file__))[0]
    return logging.getLogger(app_name)


def main():
    """Main function of SMRT_PreProcessing command.
    """
    args = parse_args()
    if args.verbose > 0:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    output_dir = args.output
    os.makedirs(output_dir, exist_ok=True)
    logger = logging_setup(output_dir, level=log_level)

    # Load optional configuration
    config = None
    if args.config is not None:
        if os.path.isfile(args.config):
            with open(args.config, "r") as fp:
                config = yaml.load(fp, Loader=yaml.SafeLoader)
            if "sensor_vectorization" not in config:
                logger.warning(
                    "Did not find parameters for sensor_vectorization in the config file %s. "
                    "Use default setting",
                    args.config,
                )
                config = None
            else:
                config = config["sensor_vectorization"]
        else:
            logger.warning(
                "Cannot find the specified configuration file %s. Use default settings.",
                args.config,
            )
    else:
        config = {}

    logger.info("=== SMRT Preprocessing ===")
    dataset, sequence, observation = SMRT_Preprocessing(
        args.dataset, args.output, verbose=args.verbose
    )

    logger.info("=== SMRT Sensor Vectorization ===")
    embeddings = SMRT_SensorVectorization(
        dataset, observation, sequence, args.output, config=config.get("sensor_vectorization", None)
    )

    logger.info("=== SMRT: Resident Tracking ===")
    predicted_association = smrt_tracking(
        dataset,
        observation,
        embeddings,
        args.output,
        length=args.length,
        config=config.get("tracking", None)
    )

    logger.info("=== SMRT: Resident Tracking ===")
    smrt_post_processing(
        dataset,
        observation[0],
        predicted_association,
        args.output,
        config=config.get("post_processing", None),
        show_metrics=args.metrics
    )


if __name__ == "__main__":
    main()
