"""SMRT resident post processing
"""
import argparse
import logging
import os
import copy
import pickle
import ruamel.yaml as yaml
import heapq
import shutil

from pycasas.data import CASASDataset
from pycasas.data import CASASSensorType
import smrt
import smrt.metrics.mrt_metrics

# Disable eager mode
from tensorflow.python.framework.ops import disable_eager_execution

disable_eager_execution()
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "1"

# Global logger
logger = None

# Default parameters in configuration
POST_PROCESSING_PARAMS = {
    "min_length": 4,
    "overlap": 2,
    "max_time_interval": 1800,
    "max_num_events_to_look": None,
    "show_metrics": False
}


def parse_args() -> argparse.Namespace:
    """Parse command line arguments to smrt.
    """
    parser = argparse.ArgumentParser(
        prog="smrt_sensor_vectorization", description="sMRT: Sensor Vectorization"
    )
    parser.add_argument(
        "-d",
        "--dataset",
        required=True,
        help="Path to pickled dataset file generated during pre-processing step.",
    )
    parser.add_argument(
        "-c", "--config", default=None, required=False, help="SMRT configuration file"
    )
    parser.add_argument(
        "-b",
        "--observation",
        required=True,
        help="Path to pickled sensor observation file generated during pre-processing step.",
    )
    parser.add_argument(
        "-a",
        "--association",
        required=True,
        help="Saved track-to-observation association."
    )
    parser.add_argument(
        "-o",
        "--output",
        required=True,
        help="Directory to save temporary sensor vectorization file.",
    )
    parser.add_argument(
        "-m", "--metrics", action="store_true", default=False, help="Computing SMRT metrics."
    )
    parser.add_argument("-v", "--verbose", action="count", default=0, help="Logging verbosity.")
    parser.add_argument("--version", action="version", version="%(prog)s " + smrt.version)
    return parser.parse_args()


def logging_setup(output_dir: str, filename: str = "smrt.log", level: int = logging.INFO,) -> None:
    """Setup logging.

    Args:
        output_dir: Directory to save logs.
        filename: Filename of the log.
        level: Verbosity level.
    """
    formatter = logging.Formatter(
        "[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} " "%(levelname)s - %(message)s",
        "%m-%d %H:%M:%S",
    )
    console = logging.StreamHandler()
    console.setLevel(level)
    console.setFormatter(formatter)
    file_handler = logging.FileHandler(os.path.join(output_dir, filename))
    file_handler.setLevel(level)
    file_handler.setFormatter(formatter)
    root_logger = logging.getLogger()
    root_logger.addHandler(console)
    root_logger.addHandler(file_handler)
    app_name = os.path.splitext(os.path.basename(__file__))[0]
    return logging.getLogger(app_name)


def smrt_print_metrics(
    event_list,
    min_length=5,
    max_undetected_period=50,
    title="SMRT Performance"
):
    result = smrt.metrics.mrt_metrics.mrta(
        event_list, min_length=min_length, max_undetected_period=max_undetected_period
    )
    scores = {
      'mrta': 1 - (result['fp'] + result['miss'] + result['mismatch']) / result['total'],
      'misses': result['miss'] / result['total'],
      'false positives': result['fp'] / result['total'],
      'mismatches': result['mismatch'] / result['total']
    }
    metrics = ['mrta', 'misses', 'false positives', 'mismatches']
    performance_str = title + ":\n"
    for metric in metrics:
        performance_str += "%s: %.4f\n" % (metric, scores[metric])
    logging.info(performance_str)
    print(performance_str)


def smrt_post_processing(
    dataset, observations, association, output_dir, config=None, show_metrics=False
):
    """SMRT post processing

    Provided with CASAS dataset, sensor observations, track/observation association generated
    by sMRT, and SMRT post processing configuration in config structure, this process analyzes the
    track association and generates sensor events associated with each identified sensors.

    The process will create a "tracks" folder containing a sensor events list of each track
    identified.

    Args:
        dataset: CASAS dataset.
        observations: sensor observations.
        embeddings: sensor embeddings.
        output_dir: Output directory to save the output file.
        length: Number of steps to run tracking.
        config: Model configuration.

    Returns:
        None
    """
    assert isinstance(dataset, CASASDataset)
    if config is None:
        config = POST_PROCESSING_PARAMS
    else:
        for param_name in POST_PROCESSING_PARAMS:
            if param_name not in config:
                config[param_name] = POST_PROCESSING_PARAMS[param_name]
    # Generate event list
    event_list = smrt.metrics.mrt_metrics.group_prediction_by_events(observations, association)
    event_by_track = smrt.metrics.mrt_metrics.group_events_by_track(event_list)
    # Filter tracks based on minimum length requirement
    track_id_to_remove = []
    for track_id in event_by_track:
        if len(event_by_track[track_id]) < config["min_length"]:
            track_id_to_remove.append(track_id)
    print(track_id_to_remove)
    for track_id in track_id_to_remove:
        del event_by_track[track_id]
    # Todo: Group tracks if there is enough overlap between the previous die and the spawn of next
    # First, order track by their starting time and end time
    # Second, identify track ID that can be merged

    def get_track_last_event_timestamp(track_id):
        return event_by_track[track_id][-1]["start"]

    valid_tracks = list(event_by_track.keys())
    num_valid_tracks = len(valid_tracks)

    tracks_ordered_by_last_event_timestamp = sorted(
        valid_tracks, key=get_track_last_event_timestamp
    )
    dead_track_search_id = 0

    track_merge_dict = {}

    active_tracks = []
    dead_tracks_to_search = []  # Ordered from most recent to the eldest
    dead_tracks_info_dict = {}

    merged_event_list = []
    unassociated_events = []

    for event_index, event in enumerate(event_list):
        tracks = event["tracks"]
        # Remove dead tracks if too old
        dead_tracks_to_remove = []
        for dead_tid in dead_tracks_to_search:
            if config["max_num_events_to_look"] is not None:
                if event_index - dead_tracks_info_dict[dead_tid]["event_id"] > dead_tid:
                    dead_tracks_to_remove.append(dead_tid)
            if config["max_time_interval"] is not None:
                if (
                    event["start"] - dead_tracks_info_dict[dead_tid]["timestamp"]
                ).total_seconds() > config["max_time_interval"]:
                    dead_tracks_to_remove.append(dead_tid)
        for dead_tid in dead_tracks_to_remove:
            if dead_tid in dead_tracks_info_dict:
                del dead_tracks_info_dict[dead_tid]
                dead_tracks_to_search.remove(dead_tid)
        # Update dead tracks info
        while dead_track_search_id < num_valid_tracks and get_track_last_event_timestamp(
            tracks_ordered_by_last_event_timestamp[dead_track_search_id]
        ) <= event["start"]:
            dead_track_id = tracks_ordered_by_last_event_timestamp[dead_track_search_id]
            dead_tracks_to_search.insert(0, dead_track_id)
            dead_tracks_info_dict[dead_track_id] = {
                "timestamp": get_track_last_event_timestamp(dead_track_id),
                "sensor": event_by_track[dead_track_id][-1]["sensor"],
                "event_id": event_index
            }
            dead_track_search_id += 1
        for tid in tracks:
            if tid not in active_tracks:
                # This is a new track, see if it corresponds to an older track
                for dead_tid in dead_tracks_to_search:
                    if dead_tracks_info_dict[dead_tid]["sensor"] == event["sensor"]:
                        while dead_tid in track_merge_dict:
                            dead_tid = track_merge_dict[dead_tid]
                        track_merge_dict[tid] = dead_tid
                        break
                active_tracks.append(tid)
        new_tracks = []
        for tid in tracks:
            if tid in event_by_track:
                if tid in track_merge_dict:
                    tid = track_merge_dict[tid]
                if tid not in new_tracks:
                    new_tracks.append(tid)
        # merged event list
        new_event = copy.deepcopy(event)
        new_event["tracks"] = new_tracks
        if len(new_tracks) == 0:
            unassociated_events.append(new_event)
            merged_event_list.append(new_event)
        else:
            merged_event_list.append(new_event)
    # Get processed event_by_track
    processed_event_by_track = smrt.metrics.mrt_metrics.group_events_by_track(merged_event_list)
    # Update event list and regenerate event_by_track dictionary
    # Write to file
    track_output_dir = os.path.join(output_dir, "tracks")
    if os.path.isdir(track_output_dir):
        shutil.rmtree(track_output_dir)
    os.makedirs(track_output_dir, exist_ok=True)
    for track_id in event_by_track:
        with open(os.path.join(track_output_dir, "%s_events.csv" % track_id), "w") as fp:
            write_event_list_to_file(event_by_track[track_id], fp, dataset, track_id)
    # Write merged tracks
    track_output_dir = os.path.join(output_dir, "merged_tracks")
    if os.path.isdir(track_output_dir):
        shutil.rmtree(track_output_dir)
    os.makedirs(track_output_dir, exist_ok=True)
    for track_id in processed_event_by_track:
        with open(os.path.join(track_output_dir, "%s_events.csv" % track_id), "w") as fp:
            write_event_list_to_file(processed_event_by_track[track_id], fp, dataset, track_id)
    with open(os.path.join(track_output_dir, "unassociated_events.csv"), "w") as fp:
        write_event_list_to_file(unassociated_events, fp, dataset, "0")
    # Show metrics
    if show_metrics:
        smrt_print_metrics(event_list, title="SMRT metrics before post-processing")
        smrt_print_metrics(merged_event_list, title="SMRT metrics after post-processing")


sensor_messages_dict = {
    "Motion": ("ON", "OFF"),
    "Item": ("ABSENT", "PRESENT"),
    "Door": ("OPEN", "CLOSE"),
    "Light": ("ON", "OFF")
}


def write_event_list_to_file(event_list, fp, dataset, track_id):
    """Write events in order to file.
    """
    event_close_queue = []
    for index, event in enumerate(event_list):
        while len(event_close_queue) > 0 and event_close_queue[0][0] <= event["start"]:
            timestamp, _, close_event = heapq.heappop(event_close_queue)
            event_string = sensor_event_format(close_event, "stop", dataset, track_id)
            fp.write(event_string)
        event_string = sensor_event_format(event, "start", dataset, track_id)
        fp.write(event_string)
        heapq.heappush(event_close_queue, (event["stop"], index, event))
    while len(event_close_queue) > 0:
        timestamp, _, close_event = heapq.heappop(event_close_queue)
        event_string = sensor_event_format(close_event, "stop", dataset, track_id)
        fp.write(event_string)


def sensor_event_format(event, start_or_stop, dataset, track_id):
    if start_or_stop == "start":
        message_id = 0
    else:
        message_id = 1
    str_timestamp = event[start_or_stop].strftime("%m/%d/%Y %H:%M:%S.%f")
    str_timezone = event[start_or_stop].strftime("%z")
    str_timezone = str_timezone[:3] + ":" + str_timezone[3:]
    sensor = dataset.sensor_list[event["sensor"]]
    sensor_type = CASASSensorType.get_best_category_for_sensor(sensor["types"])
    sensor_message = sensor_messages_dict[sensor_type][message_id]
    return "%s %s,%s,%s,%s,,%s\n" % (
        str_timestamp,
        str_timezone,
        sensor["name"],
        sensor_message,
        "T" + str(track_id),
        sensor_type
    )


def main():
    """Main function of SMRT_tracking post processing.
    """
    global logger

    args = parse_args()
    if args.verbose > 0:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    output_dir = args.output
    os.makedirs(output_dir, exist_ok=True)
    logger = logging_setup(output_dir, level=log_level)
    logger.info("=== SMRT: Post Processing ===")
    if not os.path.isfile(args.dataset):
        logger.error("Cannot find preprocessed dataset file %s. Abort.", args.dataset)
        exit(1)
    if not os.path.isfile(args.observation):
        logger.error(
            "Cannot find preprocessed sensor observation file %s. Abort.", args.observation
        )
        exit(1)
    if not os.path.isfile(args.association):
        logger.error("Cannot find track association file %s. Abort", args.association)
        exit(1)

    # Load Dataset from pickle file
    dataset = CASASDataset.load(args.dataset)

    # Load sensor observation
    with open(args.observation, "rb") as fp:
        observations = pickle.load(fp)
    logger.info("Load sensor observations from checkpoint.")

    # Load association result
    with open(args.association, "rb") as fp:
        association = pickle.load(fp)
    logger.info("Load track association from checkpoint.")

    # Load optional configuration
    config = None
    if args.config is not None:
        if os.path.isfile(args.config):
            with open(args.config, "r") as fp:
                config = yaml.load(fp, Loader=yaml.SafeLoader)
            if "post_processing" not in config:
                logger.warning(
                    "Did not find parameters for post_processing in the config file %s. "
                    "Use default settings.",
                    args.config,
                )
                config = None
            else:
                config = config["post_processing"]
        else:
            logger.warning(
                "Cannot find the specified configuration file %s. Use default settings.",
                args.config,
            )

    smrt_post_processing(
        dataset, observations[0], association, args.output, config=config, show_metrics=args.metrics
    )


if __name__ == "__main__":
    main()
