"""SMRT Preprocessing command loads CASAS data and generates sensor sequences
and observations from recorded sensor data.
"""

import argparse
import hashlib
import logging
import os
import pickle
from typing import Union, Tuple

from pycasas.data import CASASDataset
import smrt

# Global logger
logger = None

# Examples to print in -vv mode
ILLUSTRATION_LENGTH = 10

def parse_args() -> argparse.Namespace:
    """Parse command line arguments to smrt
    """
    parser = argparse.ArgumentParser(
        prog="smrt_preprocessing",
        description="Multi-resident tracking via SMRT"
    )
    parser.add_argument("-d", "--dataset",
                        required=True,
                        help="Path to CASAS directory.")
    parser.add_argument("-o", "--output",
                        required=True,
                        default=None,
                        help="Directory to save sensor sequence and observations.")
    parser.add_argument("-v", "--verbose",
                        action="count",
                        default=0,
                        help="Logging verbosity.")
    parser.add_argument(
        "--version",
        action="version",
        version="%(prog)s " + smrt.version
    )
    return parser.parse_args()


def logging_setup(
    output_dir: str,
    filename: str = "smrt.log",
    level: int = logging.INFO,
) -> None:
    """Setup logging.

    Args:
        output_dir: Directory to save logs.
        filename: Filename of the log.
        level: Verbosity level.
    """
    formatter = logging.Formatter(
        "[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} "
        "%(levelname)s - %(message)s",
        "%m-%d %H:%M:%S"
    )
    console = logging.StreamHandler()
    console.setLevel(level)
    console.setFormatter(formatter)
    file_handler = logging.FileHandler(os.path.join(output_dir, filename))
    file_handler.setLevel(level)
    file_handler.setFormatter(formatter)
    root_logger = logging.getLogger()
    root_logger.addHandler(console)
    root_logger.addHandler(file_handler)
    app_name = os.path.splitext(os.path.basename(__file__))[0]
    return logging.getLogger(app_name)


def CalculateDatasetDirHash(dataset_dir: str) -> Union[str, None]:
    """Calculate MD5 of CASAS directory.

    This function is used to match the temporary saved data against the
    specified CASAS dataset.

    Args:
        dataset_dir: Path to CASAS dataset directory.

    Returns:
        MD5 hash string of the dataset directory.
    """
    BLOCK_SIZE = 4096
    md5_hash = hashlib.md5()
    if not os.path.exists(dataset_dir):
        logging.error("CASAS dataset directory not found.")
        return None
    try:
        for root, _, files in os.walk(dataset_dir):
            for filename in files:
                file_path = os.path.join(root, filename)
            with open(file_path, "rb") as fp:
                for chunk in iter(lambda: fp.read(BLOCK_SIZE), b""):
                    md5_hash.update(chunk)
        return md5_hash.hexdigest()
    except Exception:
        logging.exception("Failed to compute hash for directory %s", dataset_dir)
        return None


def loadDataset(
    dataset_dir: str, temp_dir: str
) -> Tuple[CASASDataset, list, list]:
    """Load CASAS dataset from dataset directory or saved pickle files.

    Args:
        dataset_dir: Path to CASAS dataset directory.
        temp_dir: Path where temporary files are saved.

    Returns:
        A three tuple composed of CASASDataset object, sensor sequence and sensor observations.
    """
    # Load Dataset
    dataset = CASASDataset(directory=dataset_dir)
    dataset_name = dataset.get_name()
    # Check the authenticity of the temporary files
    md5_filename = os.path.join(temp_dir, dataset_name + ".md5")
    if os.path.isfile(md5_filename):
        with open(md5_filename, "r") as fp:
            md5_checksum = fp.readline().rstrip("\n")
        dataset_md5 = CalculateDatasetDirHash(dataset_dir=dataset_dir)
        dataset_authenticated = (md5_checksum == dataset_md5)
    else:
        dataset_authenticated = False
    load_dataset_from_source = True
    compute_sensor_sequence = True
    compute_sensor_observation = True
    dataset_pkl = os.path.join(temp_dir, dataset_name + ".pkl")
    sequence_pkl = os.path.join(temp_dir, dataset_name + "_seq.pkl")
    observation_pkl = os.path.join(temp_dir, dataset_name + "_obs.pkl")
    if dataset_authenticated:
        if os.path.isfile(dataset_pkl):
            load_dataset_from_source = False
            dataset = CASASDataset.load(dataset_pkl)
        if os.path.isfile(sequence_pkl):
            with open(sequence_pkl, "rb") as fp:
                sequence = pickle.load(fp)
                compute_sensor_sequence = False
        if os.path.isfile(observation_pkl):
            with open(observation_pkl, "rb") as fp:
                observations = pickle.load(fp)
                compute_sensor_observation = False
    if load_dataset_from_source:
        dataset.load_events(show_progress=True)
        dataset.save(dataset_pkl)
    dataset.summary()
    if compute_sensor_sequence:
        sequence = dataset.to_sensor_sequence()
        with open(sequence_pkl, "wb") as fp:
            pickle.dump(sequence, fp, protocol=-1)
    if compute_sensor_observation:
        observations = dataset.to_observation_track()
        with open(observation_pkl, "wb") as fp:
            pickle.dump(observations, fp, protocol=-1)
    return dataset, sequence, observations


def IllustrateSensorSequence(sequence: list,
                             dataset: Union[CASASDataset, None] = None,
                             start: int = 0,
                             N: int = 10) -> None:
    """Show N sensor activations in the sensor sequecne list.

    Args:
        sequence: sensor sequence list.
        dataset: CASASDataset object.
        start: start point of sensor sequence to plot.
        N: number of sensor sequence to print.
    """
    length = len(sequence[0])
    while start >= length:
        start -= length
    N = min(N, length - start)
    logging.info("Sensor sequence example: %d - %d" % (start, start + N))
    for i in range(start, start + N):
        print("# Time Step: %d" % i)
        if dataset is not None:
            print("[%s]: %d (%s)" % (
                sequence[1][i].strftime('%Y-%m-%d %H:%M:%S.%f'),
                sequence[0][i],
                dataset.sensor_list[sequence[0][i]]['name']
            ))
        else:
            print("[%s]: %d" % (
                sequence[1][i].strftime('%Y-%m-%d %H:%M:%S.%f'), sequence[0][i]
            ))


def IllustrateObservations(observations: list,
                           dataset: Union[CASASDataset, None] = None,
                           start: int = 0,
                           N: int = 10) -> None:
    """Show N sensor observation starting given a start time step.

    Args:
        observations: sensor observations.
        dataset: CASASDataset object.
        start: start point of sensor sequence to plot.
        N: number of sensor sequence to print.
    """
    length = len(observations[0])
    while start >= length:
        start -= length
    N = min(N, length - start)
    logging.info("Sensor observations example: %d - %d" % (start, start + N))
    for i in range(start, start + N):
        print("# Time Step: %d" % i)
        for sensor in observations[0][i]:
            obs = observations[0][i][sensor]
            if obs["start"] is None:
                start_time = "None"
            else:
                start_time = obs["start"].strftime("%Y-%m-%d %H:%M:%S.%f")
            if obs["stop"] is None:
                stop_time = "None"
            else:
                stop_time = obs["stop"].strftime("%Y-%m-%d %H:%M:%S.%f")
            if dataset is not None:
                print("  %s [%d]: Start %s; Stop %s; Resident %s" % (
                    dataset.sensor_list[sensor]["name"],
                    sensor, start_time, stop_time, obs["residents"]
                ))
            else:
                print("  [%d]: Start %s; Stop %s; Resident %s" % (
                    sensor, start_time, stop_time, obs["residents"]
                ))


def SMRT_Preprocessing(
    dataset: str, output_dir: str, verbose: int = 0
) -> Tuple[CASASDataset, list, list]:
    """SMRT Preprocessing main function

    Args:
        dataset_dir: Path to CASAS dataset directory.
        temp_dir: Path where temporary files are saved.

    Returns:
        A three tuple composed of CASASDataset object, sensor sequence and sensor observations.
    """
    dataset, sequence, observations = loadDataset(dataset, output_dir)
    if verbose > 1:
        # Print details about loaded observations
        IllustrateSensorSequence(sequence, dataset, 0, ILLUSTRATION_LENGTH)
        IllustrateObservations(observations, dataset, 0, ILLUSTRATION_LENGTH)
    return dataset, sequence, observations


def main():
    """Main function of SMRT_PreProcessing command.
    """
    args = parse_args()
    if args.verbose > 0:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    output_dir = args.output
    os.makedirs(output_dir, exist_ok=True)
    logger = logging_setup(output_dir, level=log_level)
    logger.info("=== SMRT Preprocessing ===")
    SMRT_Preprocessing(args.dataset, args.output, verbose=args.verbose)


if __name__ == "__main__":
    main()
