"""SMRT resident tracking
"""
import argparse
import logging
import numpy as np
import os
import pickle
import ruamel.yaml as yaml


from pycasas.data import CASASDataset
import smrt
from tensorflow.python.keras.utils.generic_utils import Progbar
from smrt.synthetic import CvMTGM_ID_Model
from smrt.utils.gmhdf import GMHdf5

# Disable eager mode
from tensorflow.python.framework.ops import disable_eager_execution

disable_eager_execution()
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "1"

# Global logger
logger = None


# Default parameters in configuration
TRACKING_PARAMS = {
    "birth_weight": 0.5 / 4,
    "birth_stddev": 0.1,
    "m_stddev": 0.1,
    "v_stddev": 0.1,
    "p_d": 0.9,
    "p_s": 0.9,
    "clutter_lam": 1,
    "Jmax": 1000,
}


def parse_args() -> argparse.Namespace:
    """Parse command line arguments to smrt.
    """
    parser = argparse.ArgumentParser(
        prog="smrt_sensor_vectorization", description="sMRT: Sensor Vectorization"
    )
    parser.add_argument(
        "-d",
        "--dataset",
        required=True,
        help="Path to pickled dataset file generated during pre-processing step.",
    )
    parser.add_argument(
        "-c", "--config", default=None, required=False, help="SMRT configuration file"
    )
    parser.add_argument(
        "-b",
        "--observation",
        required=True,
        help="Path to pickled sensor observation file generated during pre-processing step.",
    )
    parser.add_argument(
        "-e", "--embeddings", required=True, help="Path to normalized sensor embedding file (.npz)"
    )
    parser.add_argument(
        "-o", "--output", required=True, help="Directory to save temporary tracking results.",
    )
    parser.add_argument(
        "-l",
        "--length",
        required=False,
        default=None,
        type=int,
        help="Length of sensor observations to process.",
    )
    parser.add_argument("-v", "--verbose", action="count", default=0, help="Logging verbosity.")
    parser.add_argument("--version", action="version", version="%(prog)s " + smrt.version)
    return parser.parse_args()


def logging_setup(output_dir: str, filename: str = "smrt.log", level: int = logging.INFO,) -> None:
    """Setup logging.

    Args:
        output_dir: Directory to save logs.
        filename: Filename of the log.
        level: Verbosity level.
    """
    formatter = logging.Formatter(
        "[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} " "%(levelname)s - %(message)s",
        "%m-%d %H:%M:%S",
    )
    console = logging.StreamHandler()
    console.setLevel(level)
    console.setFormatter(formatter)
    file_handler = logging.FileHandler(os.path.join(output_dir, filename))
    file_handler.setLevel(level)
    file_handler.setFormatter(formatter)
    root_logger = logging.getLogger()
    root_logger.addHandler(console)
    root_logger.addHandler(file_handler)
    app_name = os.path.splitext(os.path.basename(__file__))[0]
    return logging.getLogger(app_name)


def translate_observations(observations, embeddings):
    """Translate sensor observations into sensor embeddings

    Args:
        observations: Sensor observations
        embeddings: Sensor embeddings of shape `[num_sensors, n_visible]`

    Returns:
        a two tuple. The former is a list of sensor observations, each in the form of shape
        `[num_obs, n_visible]`, which can be used directly with multi-target tracker.
        The latter is a list of sensor observations, each being a list of sensor ID.
    """
    vec_observations = []
    sensor_observations = []
    for observation in observations[0]:
        sensors = list(observation.keys())
        obs_matrix = np.copy(embeddings[sensors, :])
        vec_observations.append(obs_matrix)
        sensor_observations.append(sensors)
    return vec_observations, sensor_observations


def smrt_model(embeddings, b_weight, b_stddev, m_stddev, v_stddev, p_d, p_s, clutter_lam):
    """Composing SMRT Multi-resident Tracking Models

    Provided with the sensor embeddings, the dimension of the space, and other
    hyperparameters including birth weights, standard deviation of the dynamic
    model, measurement model, the birth location, the probability of target
    detection and target persistence.

    Args:
        embeddings: Sensor embeddings of shpae `[num_sensors, n_visible]`
        b_weight: Weight of each Gaussian component in the birth PHD
        b_stddev: The standard deviation of birth PHD
        m_stddev: The standard deviation of the linear Gaussian dynamic model
        v_stddev: The standard deviation of the linear Gaussian measurement model
        p_d: The target detection probability
        p_s: The target persistence probability
        clutter_lam: Parameter of the Poisson clutter process

    Returns:
        A constant velocity tracking model.
    """
    n_visible = embeddings.shape[1]
    n_sensors = embeddings.shape[0]
    birth_weights = np.ones((n_sensors,), dtype=np.float) * b_weight
    birth_means = np.block([embeddings, np.zeros_like(embeddings)])
    birth_covs = np.broadcast_to(
        np.identity(2 * n_visible), (n_sensors, 2 * n_visible, 2 * n_visible)
    ) * (b_stddev * b_stddev)
    return CvMTGM_ID_Model(
        n_visible=n_visible,
        v_stddev=v_stddev,
        m_stddev=m_stddev,
        birth=(birth_weights, birth_means, birth_covs),
        p_d=p_d,
        p_s=p_s,
        clutter_lam=clutter_lam,
        clutter_c=1 / (2 ** n_visible),
    )


def smrt_tracking(dataset, observations, embeddings, output_dir, length=None, config=None):
    """SMRT tracking

    Provided with CASAS dataset, sensor observations, sensor embeddings and SMRT model parameters
    in config structure, this process creates a SMRT resident tracking model (based on GM-PHD) and
    generates the resdient to sensor observation association.

    The process generates the following files in `output_dir`:
    - `<dataset>_gmphd_rec.hdf5`: HDF5 database file that keeps the Gaussian mixtures of residents'
    Probability Hypothesis density at each time step. It can be used to evaluate the parameter
    settings and performance of the algorithm.
    - `<dataset>_predicted_association.pkl`: Sensor observation to resident association data dumped
    in pickle format.

    Args:
        dataset: CASAS dataset.
        observations: sensor observations.
        embeddings: sensor embeddings.
        output_dir: Output directory to save the output file.
        length: Number of steps to run tracking.
        config: Model configuration.

    Returns:
        None
    """
    assert isinstance(dataset, CASASDataset)
    dataset_name = dataset.get_name()
    if config is None:
        config = TRACKING_PARAMS
    else:
        for param_name in TRACKING_PARAMS:
            if param_name not in config:
                config[param_name] = TRACKING_PARAMS[param_name]

    # Translate to vector array
    vec_observations, sensor_observations = translate_observations(observations, embeddings)
    # Determine number of steps to run tracking
    if length is None or length > len(vec_observations):
        length = len(vec_observations)
    model = smrt_model(
        embeddings=embeddings,
        b_weight=config["birth_weight"],
        b_stddev=config["birth_stddev"],
        m_stddev=config["m_stddev"],
        v_stddev=config["v_stddev"],
        p_d=config["p_d"],
        p_s=config["p_s"],
        clutter_lam=config["clutter_lam"],
    )
    Jmax = config["Jmax"]
    # Initial weights, means and covariances
    gm_weights = np.zeros((0,))
    gm_means = np.zeros((0, model.ndims))
    gm_covs = np.zeros((0, model.ndims, model.ndims))
    # Records
    record_filename = os.path.join(output_dir, dataset_name + "_gmphd_rec.hdf5")
    gmphd_record = GMHdf5(
        record_filename,
        mode="w",
        length=length,
        ndims=model.ndims,
        Jmax=Jmax,
        chunk_size=1000,
    )
    predicted_association = []
    # As the filter will run for a while, show progress bar
    progbar = Progbar(
        target=length,
        verbose=1,
        stateful_metrics=["targets", "residents"],
        unit_name="step",
    )
    for k in range(length):
        # Call the filter process
        gm_weights, gm_means, gm_covs = model.call(
            (gm_weights, gm_means, gm_covs, vec_observations[k])
        )
        # Truncate
        gm_weights, gm_means, gm_covs = model.gm_truncate(gm_weights, gm_means, gm_covs, Jmax)
        # Clustering
        gm_weights, gm_means, gm_covs = model.gm_id_cluster(gm_weights, gm_means, gm_covs)
        # Save PHD Gaussian mixtures
        gmphd_record.save_single_gm(k, np.copy(gm_weights), np.copy(gm_means), np.copy(gm_covs))
        gmphd_record.flush()
        # Resident Association
        ota = model.observation_association(gm_weights, gm_means, gm_covs, vec_observations[k])
        predicted_association.append({"resident": ota, "sensor": sensor_observations[k]})
        # Get number of residents
        residents = []
        for sensor in sensor_observations[k]:
            for r in observations[0][k][sensor]["residents"]:
                if r not in residents:
                    residents.append(r)
        progbar.update(k + 1, [("targets", np.sum(gm_weights)), ("residents", len(residents))])

    # Save pickled version of predicted association
    association_filename = os.path.join(output_dir, dataset_name + "_predicted_association.pkl")
    logging.info("Saving predicted association to %s ...", association_filename)
    with open(association_filename, "wb") as fp:
        pickle.dump(predicted_association, fp, protocol=-1)
    return predicted_association


def main():
    """Main function of SMRT_tracking command.
    """
    global logger

    args = parse_args()
    if args.verbose > 0:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    output_dir = args.output
    os.makedirs(output_dir, exist_ok=True)
    logger = logging_setup(output_dir, level=log_level)
    logger.info("=== SMRT: Resident Tracking ===")
    if not os.path.isfile(args.dataset):
        logger.error("Cannot find preprocessed dataset file %s. Abort.", args.dataset)
        exit(1)
    if not os.path.isfile(args.observation):
        logger.error(
            "Cannot find preprocessed sensor observation file %s. Abort.", args.observation
        )
        exit(1)
    if not os.path.isfile(args.embeddings):
        logger.error("Cannot find normalized sensor embeddings file %s. Abort.", args.sequence)
        exit(1)

    # Load Dataset from pickle file
    dataset = CASASDataset.load(args.dataset)

    # Load sensor observation
    with open(args.observation, "rb") as fp:
        observations = pickle.load(fp)
    logger.info("Load sensor observations from checkpoint.")

    # Load embeddings
    embeddings = np.load(args.embeddings)["arr_0"]

    # Load optional configuration
    config = None
    if args.config is not None:
        if os.path.isfile(args.config):
            with open(args.config, "r") as fp:
                config = yaml.load(fp, Loader=yaml.SafeLoader)
            if "tracking" not in config:
                logger.warning(
                    "Did not find parameters for tracking in the config file %s. "
                    "Use default settings.",
                    args.config,
                )
                config = None
            else:
                config = config["tracking"]
        else:
            logger.warning(
                "Cannot find the specified configuration file %s. Use default settings.",
                args.config,
            )

    smrt_tracking(dataset, observations, embeddings, args.output, length=args.length, config=config)


if __name__ == "__main__":
    main()
