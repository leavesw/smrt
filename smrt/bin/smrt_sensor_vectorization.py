"""SMRT sensor vectorization.
"""
import argparse
from datetime import datetime
import logging
import numpy as np
import os
import pickle
import random
import ruamel.yaml as yaml
import tensorflow as tf
import tensorflow.python.keras as keras

from pycasas.data import CASASDataset
import smrt
from smrt.learning.models.embedding import NCEModel
from smrt.utils import normalize_embeddings

# Disable eager mode
from tensorflow.python.framework.ops import disable_eager_execution

disable_eager_execution()
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "1"

# Global logger
logger = None


# Default parameters in configuration
SV_PARAMS = {
    "num_skips": 7,
    "skip_window": 14,
    "embedding_size": 4,
    "nce_num_samples": 16,
    "sv_learning_rate": 0.001,
    "sv_rho": 0.9,
    "sv_epsilon": 1e-8,
    "sv_decay": 0.0,
    "sv_num_epochs": 10,
}


def parse_args() -> argparse.Namespace:
    """Parse command line arguments to smrt.
    """
    parser = argparse.ArgumentParser(
        prog="smrt_sensor_vectorization", description="sMRT: Sensor Vectorization"
    )
    parser.add_argument(
        "-d",
        "--dataset",
        required=True,
        help="Path to pickled dataset file generated during pre-processing step.",
    )
    parser.add_argument(
        "-c", "--config", default=None, required=False, help="SMRT configuration file"
    )
    parser.add_argument(
        "-b",
        "--observation",
        required=True,
        help="Path to pickled sensor observation file generated during pre-processing step.",
    )
    parser.add_argument(
        "-s",
        "--sequence",
        required=True,
        help="Path to pickled sensor sequence file generated during pre-processing step.",
    )
    parser.add_argument(
        "-o",
        "--output",
        required=True,
        help="Directory to save temporary sensor vectorization file.",
    )
    parser.add_argument("-v", "--verbose", action="count", default=0, help="Logging verbosity.")
    parser.add_argument("--version", action="version", version="%(prog)s " + smrt.version)
    return parser.parse_args()


def logging_setup(output_dir: str, filename: str = "smrt.log", level: int = logging.INFO,) -> None:
    """Setup logging.

    Args:
        output_dir: Directory to save logs.
        filename: Filename of the log.
        level: Verbosity level.
    """
    formatter = logging.Formatter(
        "[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} " "%(levelname)s - %(message)s",
        "%m-%d %H:%M:%S",
    )
    console = logging.StreamHandler()
    console.setLevel(level)
    console.setFormatter(formatter)
    file_handler = logging.FileHandler(os.path.join(output_dir, filename))
    file_handler.setLevel(level)
    file_handler.setFormatter(formatter)
    root_logger = logging.getLogger()
    root_logger.addHandler(console)
    root_logger.addHandler(file_handler)
    app_name = os.path.splitext(os.path.basename(__file__))[0]
    return logging.getLogger(app_name)


def generate_train_pair(sequence, num_skips, skip_window):
    """Generate training dataset using skip gram models.

    Args:
        sequence: Sensor sequence.
        num_skips: Number of data point to skip.
        skip_window: Width of skip window.

    Returns:
        A two-tuple of a list of sensor ID and a list of corresponding target sensor ID.
    """
    num_windows = len(sequence) - 2 * skip_window
    size = num_windows * num_skips
    source = np.zeros(shape=(size,), dtype=np.int32)
    target = np.zeros(shape=(size,), dtype=np.int32)
    span = 2 * skip_window + 1
    for i in range(num_windows):
        context_sensors = [w for w in range(span) if w != skip_window]
        sensors_to_use = random.sample(context_sensors, num_skips)
        for j, context_sensor in enumerate(sensors_to_use):
            source[i * num_skips + j] = sequence[i + skip_window]
            target[i * num_skips + j] = sequence[i + context_sensor]
    return source, target


def SMRT_SensorVectorization(dataset, observation, sequence, output_dir, config=None):
    """SMRT Sensor vectorization process.

    Args:
        dataset: CASAS smart home dataset.
        observation: Pre-processed sensor observations.
        sequence: Pre-processed sensor sequence.
        output_dir: Output directory.
        config: Sensor vectorization parameters, parameters include
            - num_skips: Number of data to consider when generating training pairs.
            - skip_window: Number of times to re-use an input to generate a label.
            - num_sensors: Number of sensors (vocabulary size).
            - embedding_size: Dimension of the sensor embedding vector.
            - nce_num_samples: Number of negative samples to use in NCE model.
            - sv_learning_rate: Learning rate in sensor vectorization.
            - sv_rho:
            - sv_epsilon:
            - sv_decay:
            - sv_num_epochs: Number of epochs for sensor vector training process.
    """
    assert isinstance(dataset, CASASDataset)
    dataset_name = dataset.get_name()
    if config is None:
        config = SV_PARAMS
    else:
        for param_name in SV_PARAMS:
            if param_name not in config:
                config[param_name] = SV_PARAMS[param_name]

    x, y = generate_train_pair(sequence[0], config["num_skips"], config["skip_window"])
    vocab_size = len(dataset.sensor_list)
    model = NCEModel(
        vocab_size=len(dataset.sensor_list),
        embeddings_size=config["embedding_size"],
        nce_num_sampled=config["nce_num_samples"],
    )

    optimizer = keras.optimizers.RMSprop(
        lr=config["sv_learning_rate"],
        rho=config["sv_rho"],
        epsilon=config["sv_epsilon"],
        decay=config["sv_decay"],
    )

    def empty_loss(y_true, y_pred):
        return tf.constant(0.0)

    model.compile(optimizer=optimizer, loss=empty_loss)

    # Prepare tensorboard callback
    logdir = os.path.join(
        output_dir, dataset_name + "_tb", datetime.now().strftime("sv-%Y%m%d-%H%M%S")
    )
    tensorboard_callback = keras.callbacks.TensorBoard(logdir, profile_batch=0)
    os.makedirs(logdir, exist_ok=True)

    model.fit(
        x=[x.reshape([-1, 1]), y.reshape([-1, 1])],
        y=y,
        epochs=config["sv_num_epochs"],
        batch_size=64,
        callbacks=[tensorboard_callback],
    )

    sensor_x = np.arange(vocab_size)
    embeddings = model.predict((sensor_x.reshape([-1, 1]), sensor_x.reshape([-1, 1])))
    normalized_embeddings = normalize_embeddings(embeddings)
    embeddings_filename = os.path.join(output_dir, dataset_name + "_embeddings.npz")
    np.savez(embeddings_filename, normalized_embeddings)
    return normalized_embeddings


def main():
    """Main function of SMRT_PreProcessing command.
    """
    args = parse_args()
    if args.verbose > 0:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    output_dir = args.output
    os.makedirs(output_dir, exist_ok=True)
    logger = logging_setup(output_dir, level=log_level)
    logger.info("=== SMRT: Sensor Vectorization ===")
    if not os.path.isfile(args.dataset):
        logger.error("Cannot find preprocessed dataset file %s. Abort.", args.dataset)
        exit(1)
    if not os.path.isfile(args.observation):
        logger.error(
            "Cannot find preprocessed sensor observation file %s. Abort.", args.observation
        )
        exit(1)
    if not os.path.isfile(args.sequence):
        logger.error("Cannot find preprocessed sensor sequence file %s. Abort.", args.sequence)
        exit(1)
    # Load Dataset from pickle file
    dataset = CASASDataset.load(args.dataset)

    # Save sensor sequence
    fp = open(args.sequence, "rb")
    sequence = pickle.load(fp)
    fp.close()
    logger.info("Load sensor sequence from checkpoint.")

    # Save sensor observation
    fp = open(args.observation, "rb")
    observations = pickle.load(fp)
    fp.close()
    logger.info("Load sensor observations from checkpoint.")

    # Load optional configuration
    config = None
    if args.config is not None:
        if os.path.isfile(args.config):
            with open(args.config, "r") as fp:
                config = yaml.load(fp, Loader=yaml.SafeLoader)
            if "sensor_vectorization" not in config:
                logger.warning(
                    "Did not find parameters for sensor_vectorization in the config file %s. "
                    "Use default setting",
                    args.config,
                )
                config = None
            else:
                config = config["sensor_vectorization"]
        else:
            logger.warning(
                "Cannot find the specified configuration file %s. Use default settings.",
                args.config,
            )

    SMRT_SensorVectorization(dataset, observations, sequence, args.output, config=config)


if __name__ == "__main__":
    main()
