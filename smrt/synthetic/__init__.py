"""Synthetic data models for concept proof and testing
"""

from .models import LinearMTGM_Model
from .models import CvMTGM_Model
from .models import LinearMTGM_ID_Model
from .models import CvMTGM_ID_Model

__all__ = [
  "LinearMTGM_Model",
  "CvMTGM_Model",
  "LinearMTGM_ID_Model",
  "CvMTGM_ID_Model"
]
